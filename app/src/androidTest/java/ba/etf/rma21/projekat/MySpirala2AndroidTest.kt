//package ba.etf.rma21.projekat
//
//import android.widget.Toast
//import androidx.core.view.get
//import androidx.recyclerview.widget.RecyclerView
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.Espresso.onData
//import androidx.test.espresso.Espresso.onView
//import androidx.test.espresso.action.ViewActions
//import androidx.test.espresso.action.ViewActions.click
//import androidx.test.espresso.assertion.ViewAssertions.matches
//import androidx.test.espresso.contrib.NavigationViewActions
//import androidx.test.espresso.contrib.RecyclerViewActions
//import androidx.test.espresso.intent.rule.IntentsTestRule
//import androidx.test.espresso.matcher.ViewMatchers
//import androidx.test.espresso.matcher.ViewMatchers.*
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import ba.etf.rma21.projekat.view.FragmentPokusaj
//import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
//import org.hamcrest.CoreMatchers
//import org.hamcrest.CoreMatchers.*
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.hamcrest.CoreMatchers.`is` as Is
//
//
//@RunWith(AndroidJUnit4::class)
//class MySpirala2AndroidTest {
//    @get:Rule
//    val intentsTestRule = IntentsTestRule<MainActivity>(MainActivity::class.java)
//
//    @Test
//    fun prviZadatak() {
//
//        onView(withId(R.id.bottomNav)).check(matches(isDisplayed()))
//        onView(withId(R.id.predmeti)).perform(click())
//        onView(withId(R.id.odabirGrupa)).check(matches(isDisplayed()))
//        onView(withId(R.id.odabirGodina)).check(matches(isDisplayed()))
//        onView(withId(R.id.odabirPredmet)).check(matches(isDisplayed()))
//
//        Espresso.pressBack()
//        onView(withId(R.id.filterKvizova)).check(matches(isDisplayed()))
//        onView(withId(R.id.listaKvizova)).check(matches(isDisplayed()))
//        onView(withId(R.id.bottomNav)).check(matches(isDisplayed()))
//
//        onView(withId(R.id.predmeti)).perform(click())
//        onView(withId(R.id.dodajPredmetDugme)).perform(click())
//        onView(withId(R.id.tvPoruka)).check(matches(isDisplayed()))
//        Espresso.onView(ViewMatchers.withSubstring("Uspješno ste upisani u grupu"))
//
//        Espresso.pressBack()
//        onView(withId(R.id.filterKvizova)).check(matches(isDisplayed()))
//        onView(withId(R.id.listaKvizova)).check(matches(isDisplayed()))
//        onView(withId(R.id.bottomNav)).check(matches(isDisplayed()))
//
//        Espresso.onView(ViewMatchers.withId(R.id.predmeti)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGodina)).perform(ViewActions.click())
//
//
//        var godinaVrijednost = 2
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`(godinaVrijednost.toString())
//                )
//        ).perform(ViewActions.click())
//
//        Espresso.pressBack()
//
//        Espresso.onView(ViewMatchers.withId(R.id.predmeti)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`(godinaVrijednost.toString())
//                )
//        )
//
//
//    }
//
//    @Test
//    fun drugiZadatak() {
//        onView(withId(R.id.filterKvizova)).perform(click())
//        onData(allOf(Is(instanceOf(String::class.java)), Is("Svi kvizovi"))).perform(click())
//        val neupisani = KvizRepository.getAll().minus(KvizRepository.getMyKvizes())
//            //test da se ne moze kliknuti na neupisane kvizove
//            onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(neupisani[0].naziv)),
//                    hasDescendant(withText(neupisani[0].nazivPredmeta))), click())).check(matches(not(isClickable())))
//
//
//        val mojiKvizovi = KvizRepository.getMyKvizes()
//        onView(withId(R.id.filterKvizova)).perform(click())
//        onData(allOf(Is(instanceOf(String::class.java)), Is("Svi moji kvizovi"))).perform(click())
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText("Kviz 0")),
//                hasDescendant(withText("RMA"))), click()))
//
//        onView(withId(R.id.predajKviz)).check(matches(isDisplayed()))
//        onView(withId(R.id.zaustaviKviz)).check(matches(isDisplayed()))
//
//        var pitanja = PitanjeKvizViewModel().getPitanja("Kviz 0", "RMA")
//        var fragmentPokusaj = FragmentPokusaj(pitanja)
//
//
//        for(i in pitanja.indices) {
//            onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(i))
//            var opcije = pitanja[i].opcije
//            if(i == 0){
//                //da barem na jedno pitanje testiramo tacan odgovor
//                val tacan = pitanja[i].tacan
//                onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(tacan).perform(click())
//
//                //ne moze se kliknuti ponovo
//                onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(tacan).check(matches(not(isClickable())))
//            }
//            onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(0).perform(click())
//            onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(0).check(matches(not(isClickable())))
//            onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(pitanja[i].tacan).check(matches(not(isClickable())))
//        }
//
//    }
//
//}


