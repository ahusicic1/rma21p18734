package ba.etf.rma21.projekat
//
//import android.telecom.Call.Details.hasProperty
//import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.data.repositories.GrupaRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
//import junit.framework.Assert.assertEquals
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.MatcherAssert.assertThat
//import org.junit.Test
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.hamcrest.Matchers.*
//import org.junit.Before
//
//class PredmetRepositoryUnitTest {
//
//    @Test
//    fun testGetAll(){
//        val sviPredmeti = PredmetRepository.getAll()
//        assertEquals(sviPredmeti.size, 15)
//        assertThat(sviPredmeti, hasItem<Predmet>(hasProperty("naziv", Is("RMA"))))
//        assertThat(sviPredmeti, not(hasItem<Predmet>(hasProperty("godina", Is("6")))))
//    }
//
//    @Test
//    fun testUpisaniINeupisani(){
//        val sviPredmeti = PredmetRepository.getAll()
//        val upisani = PredmetRepository.getUpisani()
//        val neupisani = PredmetRepository.getNeupisani()
//
//        assertEquals(upisani.size+neupisani.size, sviPredmeti.size)
//        for(element in sviPredmeti){
//            if(upisani.contains(element)){
//                assertThat(neupisani, not(hasItem<Predmet>(hasProperty("naziv", Is(element.naziv)))))
//            }
//            else{
//                assertThat(upisani, not(hasItem<Predmet>(hasProperty("naziv", Is(element.naziv)))))
//            }
//        }
//    }
//
//    @Test
//    fun testUpis(){
//        val sviPredmeti = PredmetRepository.getAll()
//        val upisani = PredmetRepository.getUpisani()
//        val neupisani = PredmetRepository.getNeupisani()
//        val neupisaniSize = neupisani.size
//        val upisaniSize = upisani.size
//        val dodaj = neupisani.get(0)
//        PredmetRepository.upisiNaPredmet(neupisani.get(0))
//
//        assertEquals(PredmetRepository.getUpisani().size, upisaniSize+1)
//        assertEquals(PredmetRepository.getNeupisani().size, neupisaniSize-1)
//
//        assertThat(PredmetRepository.getUpisani(), hasItem<Predmet>(hasProperty("naziv", Is(dodaj.naziv))))
//        assertThat(PredmetRepository.getNeupisani(), not(hasItem<Predmet>(hasProperty("naziv", Is(dodaj.naziv)))))
//
//        PredmetRepository.setUpisani(listOf(
//            Predmet("RMA", 2),
//            Predmet("ORM", 2),
//            Predmet("RA", 2),
//            Predmet("OOAD", 2)
//        ))
//
//    }
//
//    @Test
//    fun testPredmetiPoGodinama(){
//        val predmetiSaPrve = PredmetRepository.getPredmetsByGodina(1)
//        for(predmet in predmetiSaPrve){
//            assertEquals(predmet.godina, 1)
//        }
//    }
//
//    @Test
//    fun testNeupisanoPoGodinama(){
//        val neupisaniSaDruge = PredmetRepository.getNeupisaniZaGodinu(2)
//        for(predmet in neupisaniSaDruge){
//            assertEquals(predmet.godina, 2)
//            assertThat(PredmetRepository.getUpisani(), not(hasItem<Predmet>(hasProperty("naziv", Is(predmet.naziv)))))
//            assertThat(PredmetRepository.getNeupisani(), hasItem<Predmet>(hasProperty("naziv", Is(predmet.naziv))))
//        }
//    }
//
//    @Test
//    fun testSet(){
//        val prije =    listOf(
//            Predmet("RMA", 2),
//            Predmet("ORM", 2),
//            Predmet("RA", 2),
//            Predmet("OOAD", 2)
//        )
//        PredmetRepository.upisiNaPredmet(Predmet("Moja okolina", 3))
//        PredmetRepository.setUpisani(prije)
//        assert(prije == PredmetRepository.getUpisani())
//    }
//
//
//
//}