package ba.etf.rma21.projekat
//
//import android.service.autofill.Validators.not
//import android.telecom.Call.Details.hasProperty
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.data.repositories.GrupaRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
//import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
//import junit.framework.Assert.assertEquals
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.MatcherAssert.assertThat
//import org.junit.Test
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.hamcrest.Matchers.*
//
//class PredmetViewModelUnitTest {
//
//    @Test
//    fun testGetNeupisani(){
//        val predmetViewModel = PredmetViewModel()
//        val neupisaniVM = predmetViewModel.getNeupisani()
//
//        val neupisaniR = PredmetRepository.getNeupisani()
//        assert(neupisaniVM == neupisaniR)
//
//        assertEquals(neupisaniVM.size + PredmetRepository.getUpisani().size, PredmetRepository.getAll().size)
//    }
//
//    @Test
//    fun testNeupisaniZaGodinu(){
//        val predmetViewModel = PredmetViewModel()
//        val neupisaniSaPrve = predmetViewModel.getNeupisaniZaGodinu(1)
//        val neupisaniSaDruge = predmetViewModel.getNeupisaniZaGodinu(2)
//
//        for(predmet in neupisaniSaPrve){
//            assertEquals(predmet.godina, 1)
//        }
//        for(predmet in neupisaniSaDruge){
//            assertEquals(predmet.godina, 2)
//        }
//    }
//
//    @Test
//    fun testUpis(){
//        val predmetViewModel = PredmetViewModel()
//        val prije = PredmetRepository.getUpisani()
//        val neupisaniSaPrve = predmetViewModel.getNeupisaniZaGodinu(1)
//        val sviNeupisani = predmetViewModel.getNeupisani()
//        val neupisaniSize = sviNeupisani.size
//        val dodaj =  neupisaniSaPrve.get(0)
//
//        predmetViewModel.upisiNaPredmet(dodaj)
//
//        assertEquals(predmetViewModel.getNeupisani().size, neupisaniSize-1)
//        assertThat(predmetViewModel.getNeupisani(), not(hasItem<Predmet>(hasProperty("naziv", Is(dodaj.naziv)))))
//
//        PredmetRepository.setUpisani(listOf(
//            Predmet("RMA", 2),
//            Predmet("ORM", 2),
//            Predmet("RA", 2),
//            Predmet("OOAD", 2)
//        ))
//    }
//
//
//}