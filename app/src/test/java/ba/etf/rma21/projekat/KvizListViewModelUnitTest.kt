package ba.etf.rma21.projekat
//
//import android.telecom.Call.Details.hasProperty
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
//import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
//import junit.framework.Assert.assertEquals
//import junit.framework.Assert.assertTrue
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.CoreMatchers.not
//import org.hamcrest.MatcherAssert.assertThat
//import org.hamcrest.Matchers.*
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.junit.Test
//import java.util.*
//
//class KvizListViewModelUnitTest {
//
//    @Test
//    fun testGetMyKvizesPlusDodaj(){
//        var kvizListViewModel = KvizListViewModel()
//        val mojiKvizovi =kvizListViewModel.getMyKvizes()
//        val notTakenPrije = kvizListViewModel.getNotTaken().size
//        val futurePrijeSize = kvizListViewModel.getFuture().size
//        val activePrijeSize = kvizListViewModel.getActive().size
//        val donePrijeSize = kvizListViewModel.getDone().size
//
//
//        assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("RMA"))))
//        assertThat(mojiKvizovi, not(hasItem<Kviz>(hasProperty("nazivPredmeta", Is("TP")))))
//
//        var pocetnaVel = kvizListViewModel.getMyKvizes().size
//        var predmet = Predmet("OS", 1)
//        var brojKvizova = 0
//        kvizListViewModel.getAll().forEach { kviz -> if(kviz.nazivPredmeta == predmet.naziv && kviz.nazivGrupe=="Grupa 1") brojKvizova++ }
//        print("$brojKvizova")
//        kvizListViewModel.dodajKvizoveZaPredmet(predmet, "Grupa 1" )
//        assertEquals(kvizListViewModel.getMyKvizes().size, pocetnaVel + brojKvizova)
//
//        //jer je kviz iz OS prosao
//        assertEquals(kvizListViewModel.getNotTaken().size, notTakenPrije + 1)
//        assertEquals(kvizListViewModel.getFuture().size, futurePrijeSize)
//        assertEquals(kvizListViewModel.getActive().size, activePrijeSize)
//        assertEquals(kvizListViewModel.getDone().size, donePrijeSize)
//
//
//    }
//
//
//    @Test
//    fun testGetAll(){
//        var kvizListViewModel = KvizListViewModel()
//        val size1 = kvizListViewModel.getAll().size
//        val sviKvizovi = kvizListViewModel.getAll()
//
//
//        assertThat(sviKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("RMA"))))
//        assertThat(sviKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("TP"))))
//
//      val sortirano =  kvizListViewModel.getAll().sortedBy { it.datumPocetka }
//        assertTrue(sviKvizovi == sortirano)
//    }
//
//    @Test
//    fun testGetDone(){
//        var kvizListViewModel = KvizListViewModel()
//        val uradjeniKvizovi = kvizListViewModel.getDone()
//        val mojiKvizovi = kvizListViewModel.getMyKvizes()
//        for(element in uradjeniKvizovi){
//            assert(element.osvojeniBodovi != null)
//            assert(element.datumPocetka <= Calendar.getInstance().time)
//            assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta))))
//        }
//    }
//
//    @Test
//    fun testGetFuture(){
//        var kvizListViewModel = KvizListViewModel()
//        val buduciKvizovi = kvizListViewModel.getFuture()
//        val mojiKvizovi = kvizListViewModel.getMyKvizes()
//        for(element in buduciKvizovi){
//            assert(element.osvojeniBodovi == null)
//            assert(element.datumPocetka > Calendar.getInstance().time)
//            assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta))))
//        }
//    }
//
//    @Test
//    fun testGetNotTaken(){
//        var kvizListViewModel = KvizListViewModel()
//        val neuradjeniKvizovi = kvizListViewModel.getNotTaken()
//        val mojiKvizovi = kvizListViewModel.getMyKvizes()
//        for(element in neuradjeniKvizovi){
//            assert(element.osvojeniBodovi == null)
//            assert(element.datumKraj <= Calendar.getInstance().time)
//            assert(element.osvojeniBodovi == null)
//            assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta))))
//        }
//    }
//
//    @Test
//    fun testGetActive(){
//        var kvizListViewModel = KvizListViewModel()
//        val aktivniKvizovi = kvizListViewModel.getActive()
//        val mojiKvizovi = kvizListViewModel.getMyKvizes()
//        for(element in aktivniKvizovi){
//            assert(element.osvojeniBodovi == null)
//            assert(element.datumPocetka<=Calendar.getInstance().time && element.datumKraj > Calendar.getInstance().time)
//            assert(element.osvojeniBodovi == null)
//            assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta))))
//        }
//    }
//
//
//
//}