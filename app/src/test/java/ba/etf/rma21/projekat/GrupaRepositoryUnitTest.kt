package ba.etf.rma21.projekat
//
//import android.service.autofill.Validators.not
//import android.telecom.Call.Details.hasProperty
//import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.data.repositories.GrupaRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
//import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
//import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
//import junit.framework.Assert.assertEquals
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.MatcherAssert.assertThat
//import org.junit.Test
//import org.hamcrest.Matchers.*
//import org.junit.Before
//import org.hamcrest.CoreMatchers.`is` as Is
//
//class GrupaRepositoryUnitTest {
//
//    @Test
//    fun testGroupsByPredmet(){
//        var grupeRMA = GrupaRepository.getGroupsByPredmet("RMA")
//        assertEquals(grupeRMA.size, 3)
//        assertEquals(GrupaRepository.getGroupsByPredmet("LD").size, 0)
//        assertThat(grupeRMA, hasItem<Grupa>(hasProperty("naziv", Is("Grupa 3"))))
//        assertThat(grupeRMA, not(hasItem<Grupa>(hasProperty("naziv", Is("Grupa 4")))))
//        for(grupa in grupeRMA){
//            assert(grupa.nazivPredmeta == "RMA" )
//        }
//    }
//
//    @Test
//    fun testMojeGrupePlusUpis(){
//        var mojeGrupe = GrupaRepository.getMojeGrupe()
//        val pocetne = mojeGrupe
//println("predmeta ${PredmetRepository.getUpisani().size}, mojih grupa: ${mojeGrupe.size}")
//        for(predmet in PredmetRepository.getUpisani()){
//            var brojac = 0
//            for(grupa in mojeGrupe){
//                if(grupa.nazivPredmeta == predmet.naziv) brojac++
//            }
//            assertEquals(brojac, 1)
//        }
//
//        var predmet = Predmet("TP", 1)
//        var sizePrije = mojeGrupe.size
//        GrupaRepository.upisiUGrupu(GrupaRepository.getGroupsByPredmet(predmet.naziv).get(0))
//
//        assertEquals(GrupaRepository.getMojeGrupe().size, sizePrije+1)
//        GrupaRepository.setMojeGrupe(listOf(
//            Grupa("Moja grupa", "RMA"),  Grupa("Moja grupa", "ORM"),
//            Grupa("Moja grupa", "OOAD"),  Grupa("Moja grupa", "RA")))
//    }
//
//    @Test
//    fun testSet(){
//        var pocetak = listOf(
//            Grupa("Moja grupa", "RMA"),  Grupa("Moja grupa", "ORM"),
//            Grupa("Moja grupa", "OOAD"),  Grupa("Moja grupa", "RA"))
//        GrupaRepository.upisiUGrupu(Grupa("test", "test"))
//        GrupaRepository.setMojeGrupe(pocetak)
//        assert(GrupaRepository.getMojeGrupe()==pocetak)
//    }
//
//
//}