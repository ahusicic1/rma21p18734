package ba.etf.rma21.projekat

//
//import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.data.repositories.GrupaRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
//import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
//import junit.framework.Assert.assertEquals
//import org.hamcrest.MatcherAssert.assertThat
//import org.junit.Test
//import org.hamcrest.Matchers.*
//import org.junit.Assert
//import org.hamcrest.CoreMatchers.`is` as Is
//
//
//class GrupaViewModelUnitTest {
//
//    @Test
//    fun testMojeGrupePlusUpis() {
//
//        var grupaVM = GrupaViewModel()
//
//        var mojeGrupe = grupaVM.getMojeGrupe()
//        val prije = mojeGrupe
//        var brojUpisanihPredmeta = PredmetRepository.getUpisani().size
//        Assert.assertEquals(brojUpisanihPredmeta, mojeGrupe.size)
//
//        for(predmet in PredmetRepository.getUpisani()){
//            var brojac = 0
//            for(grupa in mojeGrupe){
//                if(grupa.nazivPredmeta == predmet.naziv) brojac++
//            }
//            Assert.assertEquals(brojac, 1)
//        }
//
//        var predmet = Predmet("TP", 1)
//        var sizePrije = mojeGrupe.size
//        grupaVM.upisiUGrupu(grupaVM.getGroupsByPredmet(predmet.naziv).get(0))
//
//        assertEquals(grupaVM.getMojeGrupe().size, sizePrije+1)
//        GrupaRepository.setMojeGrupe(listOf(
//            Grupa("Moja grupa", "RMA"),  Grupa("Moja grupa", "ORM"),
//            Grupa("Moja grupa", "OOAD"),  Grupa("Moja grupa", "RA")))
//    }
//
//    @Test
//    fun testGetGroupsByPredmet() {
//        var grupaVM = GrupaViewModel()
//
//        var grupeRMA = grupaVM.getGroupsByPredmet("RMA")
//        assertEquals(grupeRMA.size, 3)
//        assertEquals(grupaVM.getGroupsByPredmet("LD").size, 0)
//        assertThat(grupeRMA, hasItem<Grupa>(hasProperty("naziv", Is("Grupa 3"))))
//        assertThat(grupeRMA, not(hasItem<Grupa>(hasProperty("naziv", Is("Grupa 4")))))
//        for(grupa in grupeRMA){
//            assert(grupa.nazivPredmeta == "RMA" )
//        }
//    }
//}