package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import junit.framework.Assert.assertEquals
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.MatcherAssert.assertThat
//import org.hamcrest.Matchers.*
//import org.junit.Test
//import java.util.*
//import org.hamcrest.CoreMatchers.`is` as Is
//
//class KvizRepositoryUnitTest {
//      @Test
//      fun testGetMyKvizes(){
//          val mojiKvizovi =KvizRepository.getMyKvizes()
//
//          assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("RMA"))))
//          assertThat(mojiKvizovi, not(hasItem<Kviz>(hasProperty("nazivPredmeta", Is("TP")))))
//      }
//
//
//    @Test
//    fun testgetAll(){
//        val sizePrije = KvizRepository.getAll().size
//        val sviKvizovi = KvizRepository.getAll()
//
//        assertThat(sviKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("RMA"))))
//        assertThat(sviKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("TP"))))
//    }
//
//    @Test
//    fun testGetDone(){
//        val uradjeniKvizovi = KvizRepository.getDone()
//        val mojiKvizovi = KvizRepository.getMyKvizes()
//        for(element in uradjeniKvizovi){
//            assert(element.osvojeniBodovi != null)
//            assert(element.datumPocetka <= Calendar.getInstance().time)
//            assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta))))
//
//        }
//    }
//
//    @Test
//    fun testGetFuture(){
//        val buduciKvizovi = KvizRepository.getFuture()
//        val mojiKvizovi = KvizRepository.getMyKvizes()
//        for(element in buduciKvizovi){
//            assert(element.osvojeniBodovi == null)
//            assert(element.datumPocetka > Calendar.getInstance().time)
//            assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta))))
//        }
//
//    }
//
//    @Test
//    fun testGetNotTaken(){
//        val neuradjeniKvizovi = KvizRepository.getNotTaken()
//        val mojiKvizovi = KvizRepository.getMyKvizes()
//        for(element in neuradjeniKvizovi){
//            assert(element.osvojeniBodovi == null)
//            assert(element.datumKraj <= Calendar.getInstance().time)
//            assert(element.osvojeniBodovi == null)
//            assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta))))
//        }
//    }
//
//    @Test
//    fun testGetActive(){
//        val aktivniKvizovi = KvizRepository.getActive()
//        val mojiKvizovi = KvizRepository.getMyKvizes()
//        for(element in aktivniKvizovi){
//            assert(element.osvojeniBodovi == null)
//            assert(element.datumPocetka<=Calendar.getInstance().time && element.datumKraj > Calendar.getInstance().time)
//            assert(element.osvojeniBodovi == null)
//            assertThat(mojiKvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta))))
//
//        }
//    }
//
//
//    @Test
//    fun testGetOstali(){
//        val ostaliKvizovi = KvizRepository.getOstali()
//        val mojiKvizovi = KvizRepository.getMyKvizes()
//        for(element in ostaliKvizovi){
//
//            assertThat(mojiKvizovi, not(hasItem<Kviz>(hasProperty("nazivPredmeta", Is(element.nazivPredmeta)))))
//        }
//    }
//
//
//}