package ba.etf.rma21.projekat.viewmodel


import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import kotlinx.coroutines.*

class GrupaViewModel {

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun dajMojeGrupe(context: Context, onSuccess: (grupe: List<Grupa>) -> Unit,
                     onError: () -> Unit){
        scope.launch{
            val result = PredmetIGrupaRepository.dajMojeGrupe(context)
            when (result) {
                is List<Grupa> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

    fun writeGrupa(context: Context, grupa:Grupa, onSuccess: (movies: String) -> Unit,
                onError: () -> Unit){
        scope.launch{
            val result = PredmetIGrupaRepository.writeGrupa(context,grupa)
            when (result) {
                is String -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

    fun getGrupe(onSuccess: (grupe: List<Grupa>) -> Unit,
                 onError: () -> Unit){
        scope.launch{
            val result = PredmetIGrupaRepository.getGrupe()
            when (result) {
                is List<Grupa> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

        fun getGroupsByPredmet(idPredmeta: Int, onSuccess: (grupe: Array<*>) -> Unit,
                               onError: () -> Unit){
            scope.launch{

                // Make the network call and suspend execution until it finishes
                val     result = PredmetIGrupaRepository.getGrupeZaPredmet(idPredmeta)

                // Display result of the network request to the user
                when (result) {
                    is Array<*> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }

    }

    fun upisiUGrupu(grupa: Grupa, onSuccess: (grupe: Boolean) -> Unit,
                           onError: () -> Unit){
        Log.i("onSuccessGr pozvala gVM", "upisi u grupu: " + grupa.naziv)
        scope.launch{

            // Make the network call and suspend execution until it finishes
            val     result = PredmetIGrupaRepository.upisiUGrupu(grupa.id)

            // Display result of the network request to the user
            when (result) {
                is Boolean -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }


//    fun getMojeGrupe(): List<Grupa>{
//        return GrupaRepository.getMojeGrupe()
//    }


}