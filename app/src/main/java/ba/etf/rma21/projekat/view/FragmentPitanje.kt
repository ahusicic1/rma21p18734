package ba.etf.rma21.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.core.view.get
import androidx.core.view.iterator
import androidx.core.view.size
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.PitanjeKviz
//import ba.etf.rma21.projekat.data.models.PitanjeKviz
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import com.google.android.material.navigation.NavigationView


class FragmentPitanje(pitanje: Pitanje): Fragment() {
    private var pitanje: Pitanje = pitanje
    companion object{ private var pitanjeKvizVM = PitanjeKvizViewModel() }
    private lateinit var tekstPitanja: TextView
    private lateinit var navigationView: NavigationView
    private var pitanjeKvizViewModel = PitanjeKvizViewModel()
    private lateinit   var odgovoriLV: ListView
    private lateinit var fragmentPokusaj: Fragment

//    companion object {
//        fun newInstance(pitanje: Pitanje): FragmentPitanje = FragmentPitanje(pitanje)
//    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.fragment_pitanja, container, false)
        var odgovoriList: List<String> = pitanje.opcije
        odgovoriLV = view.findViewById<ListView>(R.id.odgovoriLista)
        odgovoriLV.adapter  = ArrayAdapter<String>(view.context, android.R.layout.simple_list_item_1, odgovoriList)

        tekstPitanja = view.findViewById(R.id.tekstPitanja)
        tekstPitanja.text = pitanje.tekstPitanja

          fragmentPokusaj = this.parentFragment!!
           navigationView = fragmentPokusaj.view!!.findViewById(R.id.navigacijaPitanja)
        val menu = navigationView.menu

            odgovoriLV.setOnItemClickListener(OnItemClickListener { parent, view, position, id ->
                    val o: Any = odgovoriLV.getItemAtPosition(position)
                    if (position != pitanje.tacan) {

                        odgovoriLV.get(position)
                            .setBackgroundColor(
                                ContextCompat.getColor(
                                    inflater.context,
                                    R.color.red
                                )
                            )
                        // pitanjeKvizViewModel.oznaciTacno(pitanje)

                    }

                        odgovoriLV.get(pitanje.tacan)
                            .setBackgroundColor(
                                ContextCompat.getColor(
                                    inflater.context,
                                    R.color.green
                                )
                            )

                    pitanjeKvizVM.dodajMojOdgovor(pitanje, odgovoriList[position])
                    odgovoriLV.isEnabled = false


                var kviz = arguments?.getString("nazivKviza")
                var predmet = pitanjeKvizVM.dajPredmet(pitanje, kviz!!)
               if((activity as MainActivity).getBottomNav().selectedItemId != R.id.predajKviz) {
                    pitanjeKvizVM.getKvizPitanja(kviz, predmet).forEach {
                        val p = pitanjeKvizVM.getPitanje(it.naziv)
                        if (it.odgovor != "") {
                            if (it.odgovor.equals(p.opcije[p.tacan])) {
                                var item = (parentFragment!! as FragmentPokusaj).navigacijaPitanja.menu.getItem(FragmentPokusaj.indeks)
                                val s = SpannableString((FragmentPokusaj.indeks + 1).toString())
                                s.setSpan(ForegroundColorSpan(Color.parseColor("#3DDC84")), 0, s.length, 0)
                                item.setTitle(s)
                            } else {
                                var item = (parentFragment!! as FragmentPokusaj).navigacijaPitanja.menu.getItem(FragmentPokusaj.indeks)
                                val s = SpannableString((FragmentPokusaj.indeks + 1).toString())
                                s.setSpan(ForegroundColorSpan(Color.parseColor("#DB4F3D")), 0, s.length, 0)
                                item.setTitle(s)
                            }

                        }
                    }
                }


             //   }
            })

        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler(Looper.getMainLooper()).postDelayed({
            var kviz = arguments?.getString("nazivKviza")
            var predmet = pitanjeKvizVM.dajPredmet(pitanje, kviz!!)
            var pk: PitanjeKviz? = pitanjeKvizVM.getPitanjeKviz( kviz , predmet, pitanje.naziv)

            if(pk!=null) {
                if (pk!!.odgovor != "" || (activity as MainActivity).getBottomNav().selectedItemId == R.id.predajKviz) {
                    var indeks = pitanjeKvizVM.dajIndeksMogOdgovora(pitanje)

                    odgovoriLV.performItemClick(odgovoriLV.getAdapter().getView(indeks, null, null),
                            indeks, odgovoriLV.getItemIdAtPosition(indeks))
                }
            }
        } ,10)
    }

    private fun getPozicija(item: MenuItem?, vel: Int): Int{
        if(item == null) return -1
        for(i in 1..vel){
            if(item.toString()==i.toString()) return i
        }
        return 0
    }

    private fun setTextColorForMenuItem(
        menuItem: MenuItem,
        @ColorRes color: Int
    ) {
        val spanString = SpannableString(menuItem.title.toString())
        spanString.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(activity!!, color)),
            0,
            spanString.length,
            0
        )
        menuItem.title = spanString
    }

    private fun resetAllMenuItemsTextColor(navigationView: NavigationView) {
        for (i in 0 until navigationView.menu.size()) setTextColorForMenuItem(
            navigationView.menu.getItem(i),
            R.color.black
        )
    }


}