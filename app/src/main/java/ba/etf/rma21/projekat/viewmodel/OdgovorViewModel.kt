package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class OdgovorViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)
    fun predajOdgovore(idKviz: Int){
        scope.launch {
            var result = OdgovorRepository.predajOdgovore(idKviz)

        }
    }
}