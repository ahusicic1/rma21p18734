package ba.etf.rma21.projekat

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import ba.etf.rma21.projekat.data.models.KvizResponse
import ba.etf.rma21.projekat.data.models.PredmetResponse
//import ba.etf.rma21.projekat.data.repositories.GrupaRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import ba.etf.rma21.projekat.view.FragmentPokusaj
import ba.etf.rma21.projekat.view.KvizListAdapter
import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PredmetViewModel


class UpisPredmet : AppCompatActivity() {
    private lateinit var odabirGodina: Spinner
    private lateinit var odabirPredmet: Spinner
    private lateinit var odabirGrupa: Spinner
    private   lateinit var listPredmeti: List<PredmetResponse>
    private lateinit var dodajPredmetDugme: Button
    private var kvizListViewModel = KvizListViewModel()
    private var predmetViewModel = PredmetViewModel()
    private var grupaViewModel = GrupaViewModel()
    private lateinit var listaKvizovaAdapter: KvizListAdapter // TODO

    companion object{
        var zadnjaGodina: Int = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.upis_predmet)

        odabirGodina = findViewById(R.id.odabirGodina)
        odabirPredmet = findViewById(R.id.odabirPredmet)
        odabirGrupa = findViewById(R.id.odabirGrupa)
        dodajPredmetDugme = findViewById(R.id.dodajPredmetDugme)
        ///////////////////////

        ///listener za dugme
        listaKvizovaAdapter = KvizListAdapter(this, listOf())

        dodajPredmetDugme.isEnabled=true
        dodajPredmetDugme.setOnClickListener {

//popraviti
//          var odabraniPredmet = predmetViewModel.getNeupisaniZaGodinu(odabirGodina.selectedItem.toString().toInt())
//              .find { p -> p.naziv == odabirPredmet.selectedItem.toString() }
//
//            var odabranaGrupa = grupaViewModel.getGroupsByPredmet(odabraniPredmet!!.naziv).
//            find { grupa -> grupa.nazivPredmeta == odabraniPredmet!!.naziv }
////
//             predmetViewModel.upisiNaPredmet(odabraniPredmet!!)
//             grupaViewModel.upisiUGrupu(odabranaGrupa!!)
//             kvizListViewModel.dodajKvizoveZaPredmet(odabraniPredmet, odabranaGrupa.naziv)
//             zadnjaGodina = odabirGodina.selectedItemPosition
//
//            val intent = Intent(this, MainActivity::class.java)  //TODO vidjeti treba li ikako vracati u main
//            startActivity(intent)
        }

//////////////////////////////
        ArrayAdapter.createFromResource(
            this,
            R.array.godina,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            odabirGodina.adapter = adapter
            odabirGodina.setSelection(zadnjaGodina)
        }

      //odabirGrupa.isEnabled = true

        odabirGodina.onItemSelectedListener = object :

            AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                       // dodajPredmetDugme.isEnabled=false
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                //popraviti
//               // zadnjaGodina = odabirGodina.selectedItemPosition
//                dodajPredmetDugme.isEnabled =
//                    !predmetViewModel.getNeupisaniZaGodinu(odabirGodina.selectedItem.toString().toInt()).isEmpty()
//
//                    var stringPredmeti = PredmetRepository.getNeupisaniZaGodinu(
//                        odabirGodina.selectedItem.toString().toInt()
//                    ).stream().map { it.naziv }.toArray()
//                    val predmetAdapter = ArrayAdapter(
//                        this@UpisPredmet,
//                        android.R.layout.simple_spinner_item,
//                        stringPredmeti
//                    )
//                    predmetAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                    odabirPredmet.adapter = predmetAdapter


            }

        } //kraj


        odabirPredmet.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                   dodajPredmetDugme.isEnabled=false
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                   // dodajPredmetDugme.isEnabled = (odabirPredmet.selectedItem != null && odabirGrupa.selectedItem!=null)
// popraviti
//                        var grupe =
//                            GrupaRepository.getGroupsByPredmet(odabirPredmet.selectedItem.toString())
//                                .stream().map { it.naziv }.toArray()
//
//                    dodajPredmetDugme.isEnabled = !grupe.isEmpty()
//
//                        val grupaAdapter = ArrayAdapter(
//                            this@UpisPredmet,
//                            android.R.layout.simple_spinner_item,
//                            grupe
//                        )
//                        grupaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                        odabirGrupa.adapter = grupaAdapter

                }

            }

        odabirGrupa.onItemSelectedListener = object :
          AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                dodajPredmetDugme.isEnabled = odabirGrupa.selectedItem != null
            }

        }


    }

    private fun showKvizDetails(kviz: KvizResponse){
        val intent = Intent(this, FragmentPokusaj::class.java).apply {
            putExtra("kviz_naziv", kviz.naziv)
        }
        startActivity(intent)
    }
}