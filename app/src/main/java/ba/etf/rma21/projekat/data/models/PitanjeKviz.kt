package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName


data class PitanjeKviz ( @SerializedName("KvizId") val kvizId: Int,
                         @SerializedName("PitanjeId") val pitanjeId: Int,
                        val naziv: String,
                        val kviz: String,
                        val predmetNaziv: String,
                        var odgovor: String){

}