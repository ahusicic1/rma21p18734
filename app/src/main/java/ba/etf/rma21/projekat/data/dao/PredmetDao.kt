package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet

@Dao
interface PredmetDao {
    @Query("SELECT * FROM predmet")
    suspend fun dajSvePredmete(): List<Predmet>

    @Query("SELECT * FROM predmet WHERE id=:id LIMIT 1")
    suspend fun dajPredmetSaId(id: Int): Predmet

    @Insert
    suspend fun insertAll(vararg predmet: Predmet)

    @Query("DELETE FROM pitanje")
    suspend fun obrisi()

}