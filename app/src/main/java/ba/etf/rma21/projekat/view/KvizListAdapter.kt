package ba.etf.rma21.projekat.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.*
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import ba.etf.rma21.projekat.viewmodel.TakeKvizViewModel
import kotlinx.coroutines.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*


class KvizListAdapter(
        private val context: Context, private var kvizovi: List<Kviz>
): RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {
    private var pitanjeKvizVM = PitanjeKvizViewModel()
    private var kvizListViewModel = KvizListViewModel()
    private var takeKvizVM = TakeKvizViewModel()
    // var fragmentPokusaj: FragmentPokusaj? = null
    val scope = CoroutineScope(Job() + Dispatchers.Main)
    private var nazivPredmeta: String = ""


    companion object{  var trenutnoOdabraniKviz = 0
        private var trenutnoOdabraniPredmet = ""
    }

    fun postaviNaziveUHolder(predmetiZaKviz:List<Predmet>, holder: KvizViewHolder){

        var nazivi: MutableList<String> = mutableListOf()
        predmetiZaKviz.forEach{predmet->nazivi.add(predmet.naziv)}
        var nazivPredmeta: String = ""
        for(i in nazivi.indices){
            nazivPredmeta += nazivi[i]
            if(i != predmetiZaKviz.size-1) nazivPredmeta += " "
        }

        holder.idPredmet.text = nazivPredmeta
    }
    fun onSuccess3(pitanjaNaKvizu:List<Pitanje>, holder: KvizViewHolder, position: Int){
        holder.itemView.setOnClickListener{ openFragment(FragmentPokusaj(pitanjaNaKvizu), position ) }
    }
    fun onSuccess4(bundle: Bundle, crven: Boolean){
        if(crven) bundle.putString("boja", "crveni")
    }
    fun posaljiPKG(bundle: Bundle, pkg: PredmetKvizGrupa) {
        bundle.putString("nazivPredmeta", pkg.nazivPredmeta)
        bundle.putInt("idKviza", pkg.kvizId)
        trenutnoOdabraniPredmet = pkg!!.nazivPredmeta
    }

    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view =LayoutInflater
                .from(context)
                .inflate(R.layout.item_kviz, parent, false)

        return KvizViewHolder(view)
    }

    override fun getItemCount(): Int = kvizovi.size


    private fun postavi(holder: KvizViewHolder, position: Int, bodovi: String, boja: String){
        val pattern = "dd.MM.yyyy"
        val simpleDateFormat = SimpleDateFormat(pattern)

        if(boja=="plava"){
            holder.idDatum.text = simpleDateFormat.format(kvizovi[position].datumPocetka)
        }
        else if(boja == "zuta"){
            holder.idDatum.text = simpleDateFormat.format(kvizovi[position].datumPocetka)
        }
        else{
            if(kvizovi[position].datumKraj==null) holder.idDatum.text=""
            else holder.idDatum.text = simpleDateFormat.format(kvizovi[position].datumKraj)
        }
        holder.idBodovi.text = bodovi

        val contextMatch: Context = holder.idStatus.getContext()
        var id: Int = contextMatch.getResources()
                .getIdentifier(boja, "drawable", contextMatch.getPackageName() )
        if(id==0) id=context.getResources().getIdentifier(
                boja, "drawable", contextMatch.getPackageName()
        )

        holder.idStatus.setImageResource(id)
    }

    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {
       kvizListViewModel.dajPredmetZaKviz(kvizovi[position], holder, onSuccess = ::postaviNaziveUHolder, onError = ::onError)
        holder.idKviz.text = kvizovi[position].naziv
        holder.idVrijeme.text = kvizovi[position].trajanje.toString() + " min"


        pitanjeKvizVM.getPitanjaZaKvizIPostavi(kvizovi[position].id, holder, position, onSuccess = ::onSuccess3, onError = ::onError)
//        var listaPitanjaZaKviz: List<Pitanje> = pitanjeKvizVM.getPitanja(kvizovi[position].naziv, nazivPredmeta)
//        holder.itemView.setOnClickListener{ openFragment(FragmentPokusaj(listaPitanjaZaKviz), position ) }  //TODO provjeriti treba li open ili replace


        // urađen kviz, plava: u pocetim kvizovima ima kvizTake sa id-em kao i kvizovi[position]
//        if(kvizovi[position].datumKraj!=null &&  kvizovi[position].datumKraj< Calendar.getInstance().time && kvizoviTaken.find { kvizTaken -> kvizTaken.id==kvizovi[position].id  } != null ){ //urađen, plava
//            var pronadjeniKvizTake = kvizoviTaken.find { kvizTaken -> kvizTaken.id==kvizovi[position].id  }
//            postavi(holder, position, pronadjeniKvizTake!!.osvojeniBodovi.toString(), "plava")
//        }
       // var pronadjeniKvizTake = kvizoviTaken.find { kvizTaken -> kvizTaken.id==kvizovi[position].id  }

        if(takeKvizVM.sadrziLiOvajKviz(kvizovi[position]) ==true){

            var pronadjeniKvizTake = takeKvizVM.getListuPocetihKvizova().find { kvizTaken -> kvizTaken.id==kvizovi[position].id  } //kvizoviTaken.find { kvizTaken -> kvizTaken.id==kvizovi[position].id  }
            postavi(holder, position, pronadjeniKvizTake!!.osvojeniBodovi.toString(), "plava")

        }
        else{
            //zuti
                // izbaceno je ovo: && kvizovi[position].datumRada == null
            if( dajDatumIzStringa(kvizovi[position].datumPocetka) > Calendar.getInstance().time  ){
                postavi(holder, position, "", "zuta")
            }
            else if( kvizovi[position].datumKraj!=null && dajDatumIzStringa(kvizovi[position].datumKraj) < Calendar.getInstance().time ){ //myb i jednako // crvena
                postavi(holder, position, "", "crvena")
            }
            else{   // zelena
                postavi(holder, position, "", "zelena")
            }


        }
    }

    fun updateKvizovi(kvizovi: List<Kviz>){
        this.kvizovi = kvizovi
        notifyDataSetChanged()
    }


    inner class KvizViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val idStatus: ImageView = itemView.findViewById(R.id.idStatus)
        val idPredmet: TextView = itemView.findViewById(R.id.idPredmet)
        val idKviz: TextView = itemView.findViewById(R.id.idKviz)
        val idDatum: TextView = itemView.findViewById(R.id.idDatum)
        val idVrijeme: TextView = itemView.findViewById(R.id.idVrijeme)
        val idBodovi: TextView = itemView.findViewById(R.id.idBodovi)
    }

    private  fun replaceFragment(someFragment: Fragment? ) {
        val manager =
                (context as AppCompatActivity).supportFragmentManager
        val transaction = manager.beginTransaction()       //fragmentManager!!.beginTransaction()
        transaction.replace(R.id.container, someFragment!!)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    private fun openFragment(fragment: Fragment, position: Int) {
        // izbaceno: && kvizovi[position].datumRada == null
        if( dajDatumIzStringa(kvizovi[position].datumPocetka) > Calendar.getInstance().time) return
    //    if(!kvizListViewModel.getMyKvizes2().contains(kvizovi[position])) return NIJE MI JASNO STO SAM OVO STAVLJALA

        val bundle = Bundle()
       // var pkg = PredmetIGrupaRepository.getPKGZaKviz(kvizovi[position].id)
//        bundle.putString("nazivKviza", kvizovi[position].naziv)
//        bundle.putString("nazivPredmeta", "popravi ovo") //  , pkg!!.nazivPredmeta)
        kvizListViewModel.jeLiCrvenKviz(bundle, kvizovi[position],onSuccess = ::onSuccess4, onError= ::onError)


//        fragment.arguments = bundle
       bundle.putString("nazivKviza", kvizovi[position].naziv)
       trenutnoOdabraniKviz = position

        scope.launch{
            DBRepository.updateNow()
            var pkg = PredmetIGrupaRepository.getPKGZaKviz(kvizovi[position].id)
            when (pkg) {
                is PredmetKvizGrupa -> posaljiPKG(bundle, pkg)
                else-> onError()
            }
        }

        fragment.arguments = bundle

        val manager =
                (context as AppCompatActivity).supportFragmentManager
        val transaction =manager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun getNazivKviza(): String{
        if(kvizovi.size==0) return ""
        return kvizovi[trenutnoOdabraniKviz].naziv
    }
    fun getIdKviza(): Int{
        if(kvizovi.size==0) return 0
        return kvizovi[trenutnoOdabraniKviz].id
    }

    fun getNazivPredmet(): String{
        return trenutnoOdabraniPredmet
    }
    fun getKvizovi(): List<Kviz>{
        return kvizovi
    }

    private fun dajDatumIzStringa(datumString: String): Date {
        val format: String = "yyyy-mm-ddThh:mm:ss"
        val myFormat: SimpleDateFormat = SimpleDateFormat( "yyyy-mm-dd'T'hh:mm:ss")
        try {
            val reformattedStr: Date = myFormat.parse(datumString)
            return reformattedStr
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return Calendar.getInstance().time
    }

}