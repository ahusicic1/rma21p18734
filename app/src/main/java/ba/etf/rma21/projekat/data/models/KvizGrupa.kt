package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class KvizGrupa(
     @SerializedName("GrupaId") val grupaId: Int,
     @SerializedName("KvizId") val kvizId: Int
) {
}