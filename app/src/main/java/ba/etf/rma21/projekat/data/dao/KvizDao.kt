package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface KvizDao {
    @Query("UPDATE kviz SET ktid=:ktid WHERE id=:idKviza")
    suspend fun postaviKvizTakenId(idKviza: Int, ktid: Int)

    @Query("UPDATE kviz SET predan=:predan AND datumRada=:datRada WHERE id=:kid")
    suspend fun postaviPredanIDatumRada(kid: Int, predan: Boolean, datRada: String)

    @Query("SELECT * FROM kviz")
    suspend fun dajSveMojeKvizove():List<Kviz>

//    @Query("INSERT INTO kviz(id, naziv, datumPocetka, datumKraj, trajanje, predan, ktid) VALUES (:kvizId, :naziv, :datPoc, :datKraj, :trajanje, :predan, :ktid) ")
//    suspend fun insertAll(kvizId: Int, naziv:String, datPoc: String, datKraj: String, trajanje: Int, predan: Boolean, ktid:Int)
    @Insert
    suspend fun insertAll(vararg kviz: Kviz)

    @Insert
    suspend fun dodajMojKviz(vararg kviz: Kviz)

    @Query("UPDATE kviz SET zapocet=:jeLiZapocet WHERE id=:kvizId")
    suspend fun zapocniKviz(kvizId: Int, jeLiZapocet: String)

    @Query("UPDATE kviz SET datumRada=:datRada WHERE id=:kvizId")
    suspend fun postaviDatumRada(kvizId: Int, datRada: String)

    @Query("DELETE FROM kviz")
    suspend fun obrisi()

}