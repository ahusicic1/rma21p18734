package ba.etf.rma21.projekat.data.models

import androidx.room.*
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

@Entity
data class Kviz(
        @PrimaryKey @SerializedName("id") var id:Int,
        @ColumnInfo(name="naziv") @SerializedName("naziv") var naziv:String,
        @ColumnInfo(name="datumPocetka") @SerializedName("datumPocetak") @TypeConverters(Converters::class) var datumPocetka: String,
        @ColumnInfo(name="datumKraj") @SerializedName("datumKraj") @TypeConverters(Converters::class) var datumKraj: String,
        @ColumnInfo(name="trajanje") @SerializedName("trajanje") var trajanje:Int,
        @ColumnInfo(name="predan") var predan: Boolean,
        @ColumnInfo(name="ktid") var ktid: Int,
         var listaPredmeta: String,
         var osvojeniBodovi: Double?,
         var datumRada: String,
        var zapocet: String
): Serializable {
//        constructor() : this(0, "", "", "", 0,
//               "", false, null, "" )
}