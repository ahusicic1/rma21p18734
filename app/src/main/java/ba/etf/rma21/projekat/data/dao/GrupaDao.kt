package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Grupa

@Dao
interface GrupaDao {
    @Query("SELECT * FROM Grupa")
    suspend fun dajMojeGrupe(): List<Grupa>

    @Insert
    suspend fun insertAll(vararg grupe: Grupa)

    @Query("DELETE FROM grupa")
    suspend fun obrisi()
}