package ba.etf.rma21.projekat.data.repositories


import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.staticData.mojiKvizovi
import ba.etf.rma21.projekat.viewmodel.AccountViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class KvizRepository {

    companion object {
        // TODO: Implementirati
        init {
            // TODO: Implementirati
        }

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

          var lista: MutableList<Kviz> = (mojiKvizovi()).toMutableList()
          var accountVM = AccountViewModel()
       //   lateinit var otvoreniKviz: Kviz


        suspend fun getAll(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                try {
                    var response = ApiConfig.retrofit.getKvizovi().body()
                    return@withContext response!!
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Kviz>()
                }
            }
        }
        suspend fun getById(id: Int):Kviz?{
            return withContext(Dispatchers.IO) {
                try{
                    var response = ApiConfig.retrofit.getKvizSaId(id) //.body()
                    if(response.message().contains("not found")) return@withContext null
                    var kviz = response.body()
                    return@withContext kviz
            }catch (e: IllegalStateException){
                return@withContext null
            }
            }
        }

        suspend fun getKvizovi():List<Kviz>?{
            return withContext(Dispatchers.IO) {
                Log.i("predm grupa repoz", "getGrupe")
                try{
                    var response = ApiConfig.retrofit.getKvizovi().body()
                    Log.i("grupe: ", response.toString())
                    return@withContext response
                }catch (e: java.lang.IllegalStateException){
                    return@withContext emptyList<Kviz>()
                }
            }
        }

        suspend fun writeKvizovi(context: Context, kvizovi: List<Kviz>): String?{
            Log.i("kviz", "kvizRepo writeKvizovi")
            return withContext(Dispatchers.IO){
                try{
                    var db = AppDatabase.getInstance(context)
                    var kvizDao=db!!.kvizDao()

                    for(k in kvizovi){
                        Log.i("kviz" , "upisuje se")
                        kvizDao.insertAll(k)
                    }

                    return@withContext "success"
                }catch(e: java.lang.Exception){
                    return@withContext null
                }
            }
        }

        suspend fun getUpisani():List<Kviz>{ //listu svih kvizova za grupe na kojima je korisnik upisan
            return withContext(Dispatchers.IO) {
                try {
                    var mojiKvizovi = getMyKvizes()
                    return@withContext mojiKvizovi
//                    var accRepozitory = AccountRepository
//                   accRepozitory.setContext(context)
//                    var grupe = ApiConfig.retrofit.getGrupeZaStudenta(accRepozitory.getHash())
//                    var listaKvizova: MutableList<Kviz> = mutableListOf()
//                    grupe.body()?.forEach { grupa ->
//                            var kvizoviZaGrupu = ApiConfig.retrofit.getKvizoviZaGrupu(grupa.id).body()
//                            kvizoviZaGrupu?.forEach { kviz -> listaKvizova.add(kviz) }
//
//                    }
//
//                    return@withContext listaKvizova.toList()
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Kviz>()
                }
            }
        }


        suspend fun getMyKvizes(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                try {
                    var db = AppDatabase.getInstance(context)
                    var mojiKvizovi = db!!.kvizDao()
                    var response = mojiKvizovi.dajSveMojeKvizove()
                    return@withContext response!!
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Kviz>()
                }
            }
        }

        suspend fun getDone(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                try {
                    var response = getMyKvizes().filter { kviz -> kviz.osvojeniBodovi!=null }
                    return@withContext response!!
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Kviz>()
                }
            }
        }

        suspend fun getFuture(): List<Kviz> {
            return getMyKvizes().filter { kviz -> dajDatumIzStringa(kviz.datumPocetka) > Calendar.getInstance().time }
        }

        private fun dajDatumIzStringa(datumString: String): Date {
            val format: String = "yyyy-mm-ddThh:mm:ss"
            val myFormat: SimpleDateFormat = SimpleDateFormat(format)
            try {
                val reformattedStr: Date = myFormat.parse(datumString)
                return reformattedStr
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return Calendar.getInstance().time
        }

        suspend fun getNotTaken(): List<Kviz> {
            return getMyKvizes().filter { kviz ->
                ( dajDatumIzStringa(kviz.datumKraj) < Calendar.getInstance().time && kviz.osvojeniBodovi==null)
            }
        }

        suspend fun getActive(): List<Kviz>{
            return getMyKvizes().filter { kviz ->
                (dajDatumIzStringa(kviz.datumPocetka) <= Calendar.getInstance().time && dajDatumIzStringa(kviz.datumKraj) > Calendar.getInstance().time)
            }
        }
        // TODO: Implementirati i ostale potrebne metode

        suspend fun getOstali(): List<Kviz>{
            var mojiKvizovi = getMyKvizes()
            var sviKvizovi = getAll()
            return sviKvizovi.filter { kviz -> !mojiKvizovi.contains(kviz) }
        }

    }
}