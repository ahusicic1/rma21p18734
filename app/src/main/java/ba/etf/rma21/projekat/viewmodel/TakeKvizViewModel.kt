package ba.etf.rma21.projekat.viewmodel

import android.widget.Toast
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.*

class TakeKvizViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

//    fun getPocetiKvizovi2(): List<KvizTaken>?{
//        return TakeKvizRepository.getPocetiKvizovi()
//    }

    fun sadrziLiOvajKviz(kviz: Kviz): Boolean{
       return TakeKvizRepository.listaPocetihKvizovaId.contains(kviz.id)
    }
    fun getListuPocetihKvizovaId(): List<Int>{
        return TakeKvizRepository.listaPocetihKvizovaId
    }
    fun getListuPocetihKvizova(): List<KvizTaken>{
        return TakeKvizRepository.listaPocetihKvizova
    }


    fun getPocetiKvizovi(onSuccess: (kvizoviTaken: List<KvizTaken>) -> Unit,
                         onError: () -> Unit){

        scope.launch{
            var result = TakeKvizRepository.getPocetiKvizovi()
            when (result) {
                is List<KvizTaken> ->{ onSuccess?.invoke(result) }
                else-> onError?.invoke()
            }
        }
    }

}