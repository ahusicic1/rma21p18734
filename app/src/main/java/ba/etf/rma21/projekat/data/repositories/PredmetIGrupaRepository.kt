package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.models.PredmetKvizGrupa
import ba.etf.rma21.projekat.data.models.PredmetResponse
import ba.etf.rma21.projekat.data.staticData.grupe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.IllegalStateException


class PredmetIGrupaRepository {
    companion object{
        //TODO dodati ostale metode

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun writeGrupa(context: Context, grupa: Grupa): String?{
            return withContext(Dispatchers.IO){
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.grupaDao().insertAll(grupa)
                    var kvizoviZaGrupu = ApiConfig.retrofit.getKvizoviZaGrupu(grupa.id).body()
                    for(i in kvizoviZaGrupu!!.indices) {
                        db.kvizDao().dodajMojKviz(kvizoviZaGrupu[i])
                    }
                    return@withContext "success"
                }catch(e: java.lang.Exception){
                    return@withContext null
                }
            }
        }

        suspend fun writePredmet(context: Context, predmet: Predmet): String?{
            return withContext(Dispatchers.IO){
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.predmetDao().insertAll(predmet)
                    return@withContext "success"
                }catch(e: java.lang.Exception){
                    return@withContext null
                }
            }
        }

        suspend fun dajMojeGrupe(context: Context) : List<Grupa> {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var grupe = db!!.grupaDao().dajMojeGrupe()
                return@withContext grupe
            }
        }

        suspend fun getPredmeti():List<Predmet> {
            return withContext(Dispatchers.IO) {
                try {
                    var response = ApiConfig.retrofit.getPredmeti()
                    val responseBody = response.body()
                    return@withContext responseBody!!
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Predmet>()
                }
            }
        }

        suspend fun getGrupe():List<Grupa>?{
            return withContext(Dispatchers.IO) {
                Log.i("predm grupa repoz", "getGrupe")
                try{
                var response = ApiConfig.retrofit.getGrupe().body()
                    Log.i("grupe: ", response.toString())
                return@withContext response
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Grupa>()
                }
            }
        }

         suspend  fun getPKGZaKviz(idKviza: Int): PredmetKvizGrupa?{
            return withContext(Dispatchers.IO) {
              //     var predmeti = runBlocking{ getPredmeti() }
                var predmeti = getPredmeti()
                var upisaneGrupe = runBlocking{ getUpisaneGrupe() }
              //  var upisaniPredmeti: MutableList<Predmet>? = mutableListOf()
                var pkg:PredmetKvizGrupa  = PredmetKvizGrupa(0,0,0, "")
                pkg.kvizId =idKviza

                predmeti?.forEach { predmet ->

                         runBlocking {
                        var grupeZaPredmet: List<Grupa>? = ApiConfig.retrofit.getGrupeZaPredmet(predmet.id).body()
                             for(i in grupeZaPredmet!!.indices){
                                 for(j in upisaneGrupe!!.indices){
                                     if(upisaneGrupe[j]!!.id == grupeZaPredmet[i].id){
                                        // upisaniPredmeti?.add(predmet)
                                         var kvizoviZaGrupu = ApiConfig.retrofit.getKvizoviZaGrupu(upisaneGrupe[j].id).body()
                                         kvizoviZaGrupu?.forEach { kviz-> if(kviz.id==idKviza){
                                             pkg.grupaId == upisaneGrupe[j].id
                                             pkg.predmetId == predmet.id
                                             pkg.nazivPredmeta = predmet.naziv
                                         } }
                                         break }
                                 }
                             }
                    }
                }

                return@withContext pkg
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta:Int):List<Grupa>?{
            return withContext(Dispatchers.IO) {
                try {
                    var response = ApiConfig.retrofit.getGrupeZaPredmet(idPredmeta).body()
                    return@withContext response
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Grupa>()
                }
            }
        }


        suspend fun upisiUGrupu(idGrupa:Int):Boolean?{
               var poruka = GrupaRepository.upisiUGrupu(idGrupa).toString()

            Log.i("repoz predmet grupa", "upisuje u grupu " + idGrupa.toString())
            Log.i("repoz predmet grupa", "poruka: " + poruka)
            return poruka.contains("Student ahusicic1@etf.unsa.ba je dodan u grupu")
        }


        suspend fun getUpisaneGrupe():List<Grupa>?{
            return withContext(Dispatchers.IO) {
            var db = AppDatabase.getInstance(context)
            var grupe = db!!.grupaDao().dajMojeGrupe()
            return@withContext grupe
            }

//            return withContext(Dispatchers.IO) {
//                var account = ApiConfig.retrofit.getStudentSaHashom(AccountRepository.getHash())
//                var lista: MutableList<Grupa> = mutableListOf()
//                if(account.body() == null) return@withContext lista
//
//                try {
//                    var rez = ApiConfig.retrofit.getGrupeZaStudenta(AccountRepository.getHash()).body()!!
//                    for(i in rez.indices) lista.add(rez[i])
//                    //if (ApiConfig.retrofit.getGrupeZaStudenta(account.body()!!.id).isSuccessful == false) return@withContext emptyList<Grupa>()
//                }catch (e: IllegalStateException){
//                    var grupa: Grupa? = ApiConfig.retrofit.getGrupaZaStudenta(AccountRepository.getHash()).body()
//
//                    if(grupa == null) return@withContext lista
//                    lista.add(grupa!!)
//
//                    return@withContext lista
//                }
//                return@withContext lista
//            }
        }




    }
}