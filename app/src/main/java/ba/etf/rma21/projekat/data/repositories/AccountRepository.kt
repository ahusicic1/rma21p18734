package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.service.voice.AlwaysOnHotwordDetector
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*


class AccountRepository {

    companion object {
        //TODO Ovdje trebate dodati hash string vašeg accounta
        var acHash: String = "e75f79ff-ddea-4a2a-a734-9c5e314dbd9b"
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }
        fun getContext(): Context{ return context }

         suspend fun postaviHash(hash: String): Boolean {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var accountDao = db!!.accountDao()
               // var acc = accountDao.getAll()
                var defaultniDatum =Date(0,0,0)
                val formatter = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
                Log.i("danasnji datum ", formatter.format(Calendar.getInstance().time))
                if(accountDao.brojKolona() == 0){
                    Log.i("unosi se u tabelu ", hash)
                    accountDao.postaviAcc(0, hash, formatter.format(Calendar.getInstance().time))
                }
                else {
                    accountDao.updateHash(hash)
                    }

//                else if(acc[0].acHash!=acHash  ){ Log.i("else if ", "acc[0] != hash" +"(ac[0]) " + acc[0].acHash + "acHash " + acHash )
//                    accountDao.updateHash(acHash)
//                }
            //    this.acHash = acHash
                  return@withContext true
            }
        }

        suspend fun getHash(): String{
            return withContext(Dispatchers.IO){
                var db = AppDatabase.getInstance(context)
                var accountDao = db!!.accountDao()
                var hash = accountDao.getAcHash()
                if(hash==null){
                    hash= acHash
                    var defaultniDatum =Date(0,0,0)
                    val formatter = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
                    accountDao.postaviAcc(0, acHash, formatter.format(Calendar.getInstance().time))
                }
                return@withContext hash
            }
        }

//        fun getHash(): String {
//            return acHash
//        }


    }
}