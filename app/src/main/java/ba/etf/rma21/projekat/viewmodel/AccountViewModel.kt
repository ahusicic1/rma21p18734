package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.repositories.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class AccountViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun postaviHash(context: Context, hash: String ,onSuccess: () -> Unit,
                     onError: () -> Unit){
        scope.launch{
            AccountRepository.setContext(context)
            val result = AccountRepository.postaviHash(hash)
            when (result) {
                is Boolean -> onSuccess?.invoke()
                else-> onError?.invoke()
            }
        }
    }
    fun postaviHashIDatum(context: Context, hash: String ,onSuccess: () -> Unit,
                    onError: () -> Unit){
        scope.launch{
            AccountRepository.setContext(context)
            var db = AppDatabase.getInstance(context)
            var accDao = db.accountDao()
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
            if(accDao.getAll().size==0){ accDao.insertAccount(Account(0,hash, formatter.format(Calendar.getInstance().time))) }
            else { accDao.updateHash(hash)
              accDao.updateLastUpdate(formatter.format(Calendar.getInstance().time))}
            val result = accDao.getAll().size == 1
            when (result) {
                is Boolean -> onSuccess?.invoke()
                else-> onError?.invoke()
            }
        }
    }

    fun getHash(context: Context, onSuccess: () -> Unit,
                onError: () -> Unit){
        scope.launch {
            AccountRepository.setContext(context)
            val result = AccountRepository.getHash()
            when(result){
                is String -> onSuccess?.invoke()
                else -> onError?.invoke()
            }
        }
    }

}