package ba.etf.rma21.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.*
import android.widget.AdapterView
import android.widget.FrameLayout
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class FragmentPokusaj(pitanja: List<Pitanje>): Fragment() {
    lateinit var navigacijaPitanja: NavigationView
    private var pitanjaVar = pitanja
    private  var prethodniItem: MenuItem? = null
//    lateinit var menuPitanja: Menu
//    lateinit var submenu: SubMenu
      var pitanjeFragment: FragmentPitanje? = null
    private var porukaFragment: FragmentPoruka? = null
    private var pitanjeKvizVM = PitanjeKvizViewModel()
    val scope = CoroutineScope(Job() + Dispatchers.Main)
    companion object {
        var indeks = 0
        fun newInstance(pitanja: List<Pitanje>): FragmentPokusaj {
            return FragmentPokusaj(pitanja)
        }  //TODO
    }

    fun posaljiProcenat(bundle: Bundle, procenat: Double){
        bundle.putString("procenatTacnost", procenat.toString())
        porukaFragment!!.arguments = bundle
    }
    fun onSuccessPrvi(s: String) {
    }
    fun onError() {
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view: View = inflater.inflate(R.layout.fragment_pokusaj, container, false)
        navigacijaPitanja = view.findViewById(R.id.navigacijaPitanja)
         var menuPitanja = navigacijaPitanja.menu
       //   submenu = menuPitanja.addSubMenu("")

        for(i in pitanjaVar.indices){
            pitanjeKvizVM.writePitanje(
                this.requireContext(), pitanjaVar[i], onSuccess = ::onSuccessPrvi, onError = ::onError
            )
        }


        for(i in 1..pitanjaVar.size){
            navigacijaPitanja.menu.add(R.id.groupId, i-1, i-1, i.toString())
           // submenu.add(i.toString())
        }
        navigacijaPitanja.invalidate()

        if(arguments?.getString("boja") == "crveni" ) pitanjeKvizVM.postaviNetacneOdgovore(getNazivKviza(), getNazivPredmeta())

         var mOnNavigationItemSelectedListener =  NavigationView.OnNavigationItemSelectedListener { item ->
             var pozicija = getPozicija(item)
             prethodniItem = item

             var test = view.findViewById<FrameLayout>(R.id.framePitanje)
             test.removeAllViews()

            if(pozicija!=-1) {
                indeks = pozicija-1
                openChildFragment(pitanjaVar[pozicija-1], pozicija)
                return@OnNavigationItemSelectedListener true
            }
             else{
                val transaction = this.childFragmentManager.beginTransaction()
                if(porukaFragment != null){
                    transaction.remove(porukaFragment!!)
                }
                if(pitanjeFragment != null){
                    transaction.remove(pitanjeFragment!!)
                }
                porukaFragment = FragmentPoruka.newInstance()
                val bundle = Bundle()

                bundle.putString("nazivKviza", getNazivKviza())
                ///

              val predmet = pitanjeKvizVM.dajPredmet(pitanjaVar[0], getNazivKviza())
//                var procenat = pitanjeKvizVM.kolikoTacnihOdgovora(getNazivKviza(), predmet)
//                bundle.putString("procenatTacnost", procenat.toString())
                scope.launch{
                     var procenat =  pitanjeKvizVM.kolikoTacnihOdgovora(getIdKviza(), getNazivKviza(), predmet)
            when (procenat) {
                is Double -> posaljiProcenat(bundle, procenat)
                else-> onError()
            }
        }
        //        porukaFragment!!.arguments = bundle

                ///
                transaction.add(R.id.framePitanje, porukaFragment!!, "porukaFragment")
                transaction.show(porukaFragment!!)
                transaction.commit()

                return@OnNavigationItemSelectedListener true
            }
             false
        }
        navigacijaPitanja.setNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        if (savedInstanceState == null) {
           if(navigacijaPitanja.menu.size() > 0) navigacijaPitanja.getMenu().performIdentifierAction(0, 0);
        }


        val activity = activity as MainActivity
        val menu = activity.getBottomNav().menu
         menu.findItem(R.id.kvizovi).isVisible = false
         menu.findItem(R.id.predmeti).isVisible = false
        menu.findItem(R.id.predajKviz).isVisible = true
        menu.findItem(R.id.zaustaviKviz).isVisible = true


//        pitanjeKvizVM.getKvizPitanja(getNazivKviza(), getNazivPredmeta()).forEach {
//            val p = pitanjeKvizVM.getPitanje(it.naziv)
//            if(it.odgovor != ""){
//                if(it.odgovor.equals(p.opcije[p.tacan])){
//                    var item = navigacijaPitanja.menu.getItem(indeks)
//                    val s = SpannableString((indeks+1).toString())
//                    s.setSpan(ForegroundColorSpan(Color.parseColor("#3DDC84")), 0, s.length, 0)
//                    item.setTitle(s)
//                }
//                else{
//                    var item = navigacijaPitanja.menu.getItem(indeks)
//                    val s = SpannableString((indeks+1).toString())
//                    s.setSpan(ForegroundColorSpan(Color.parseColor("#DB4F3D")), 0, s.length, 0)
//                    item.setTitle(s)
//                }
//
//            }
//        }

        return view
    }

//    fun openChildFragmentPoruka() {
//        val toast = Toast.makeText(activity, "evo me", Toast.LENGTH_SHORT)
//        toast.show()
//        val transaction = this.childFragmentManager.beginTransaction()
//        if (porukaFragment != null) {
//            transaction.remove(porukaFragment!!)
//        }
//        if (pitanjeFragment != null) {
//            transaction.remove(pitanjeFragment!!)
//        }
//        porukaFragment = FragmentPoruka.newInstance()
//        val bundle = Bundle()
//        bundle.putString("nazivKviza", getNazivKviza())
//        //
////        val predmet = pitanjeKvizVM.dajPredmet(pitanjaVar[0], getNazivKviza())
////        var procenat = pitanjeKvizVM.kolikoTacnihOdgovora(getNazivKviza(), predmet)
////        bundle.putString("procenatTacnost", procenat.toString())
////        porukaFragment!!.arguments = bundle
////        transaction.add(R.id.framePitanje, porukaFragment!!, "porukaFragment")
////        transaction.show(porukaFragment!!)
////        transaction.commit()
//
//        scope.launch{
//            var procenat = pitanjeKvizVM.kolikoTacnihOdgovora(fragmentKvizovi.getAdapter()!!.getNazivKviza(), fragmentKvizovi.getAdapter()!!.getNazivPredmet())
//            when (procenat) {
//                is Double -> posaljiProcenat(bundle, procenat)
//                else-> onError()
//            }
//        }
//        //
//    }

    fun openChildFragment(pitanje: Pitanje, pozicija: Int){
        val transaction = this.childFragmentManager.beginTransaction()
        if(pitanjeFragment != null){
            transaction.remove(pitanjeFragment!!)
        }
        pitanjeFragment = FragmentPitanje(pitanjaVar[pozicija - 1])
        val bundle = Bundle()
        bundle.putString("nazivKviza", getNazivKviza())
        bundle.putInt("submenu", pitanjaVar.size)
        pitanjeFragment!!.arguments = bundle
        transaction.add(R.id.framePitanje, pitanjeFragment!!, "pitanjeFragment")
        transaction.show(pitanjeFragment!!)
        transaction.commit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val activity = activity as MainActivity
        val menu = activity.getBottomNav().menu

        menu.findItem(R.id.kvizovi).isVisible = true
        menu.findItem(R.id.predmeti).isVisible = true
        menu.findItem(R.id.predajKviz).isVisible = false
        menu.findItem(R.id.zaustaviKviz).isVisible = false
    }

     fun getPozicija(item: MenuItem): Int{
        for(i in 1..pitanjaVar.size){
            if(item.toString()==i.toString()) return i
        }
        return -1
    }

    fun getNavigationView(): NavigationView {
        return navigacijaPitanja
    }

    fun getPrethodniItem(): MenuItem?{
        return prethodniItem
    }

    fun getNazivKviza(): String{
       val nazivKviza = arguments?.getString("nazivKviza")
        return nazivKviza.toString()
    }
    fun getIdKviza(): Int{
        val idKviza = arguments?.getInt("idKviza")
        return idKviza!!.toInt()
    }
    fun getNazivPredmeta(): String{
        val nazivKviza = arguments?.getString("nazivPredmeta")
        return nazivKviza.toString()
    }

    private fun setTextColorForMenuItem(
        menuItem: MenuItem,
        @ColorRes color: Int
    ) {
        val spanString = SpannableString(menuItem.title.toString())
        spanString.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(activity!!, color)),
            0,
            spanString.length,
            0
        )
        menuItem.title = spanString
    }

    private fun resetAllMenuItemsTextColor(navigationView: NavigationView) {
        for (i in 0 until navigationView.menu.size()) setTextColorForMenuItem(
            navigationView.menu.getItem(i),
            R.color.white
        )
    }


}