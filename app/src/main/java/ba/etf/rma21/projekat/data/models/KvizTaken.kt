package ba.etf.rma21.projekat.data.models


import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

data class KvizTaken(
        @PrimaryKey @SerializedName("id") val id:Int,
        @ColumnInfo(name="KvizId") @SerializedName("KvizId") val KvizId:Int,
        @ColumnInfo(name="student")@SerializedName("student") val student:String,
        @ColumnInfo(name="osvojeniBodovi")@SerializedName("osvojeniBodovi") val osvojeniBodovi:Int,
        @ColumnInfo(name="datumRada")@SerializedName("datumRada") val datumRada:String  ) {
}