package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.*
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface Api {

    @GET("account/{id}/lastUpdate")
    suspend fun imaLiPromjena(
        @Path("id") id:String,
        @Query("date") datum: String?
    ): Response<Message>

    //get za predmet
    @GET("predmet")
    suspend fun getPredmeti(): Response<List<Predmet>>

    @GET("predmet/{id}")
    suspend fun getPredmetId(
            @Path("id") id:Int
    ):Response<Predmet>

    //grupa
    @GET("grupa")
    suspend fun getGrupe(): Response<List<Grupa>>

    @GET("grupa")
     fun getGrupe2(): Call<List<Grupa>>

    @GET("kviz")
    fun getAllKvizovi2(): Call<List<Kviz>>

    @GET("grupa/{id}")
    suspend fun getGrupaId(
            @Path("id") id: Int
    ): Response<Grupa>

    @GET("kviz/{id}/grupa")
    suspend fun getGrupeZaKviz(
            @Path("id") id: Int
    ): Response<List<Grupa>>

    @GET("/student/{id}/grupa")
    suspend fun getGrupeZaStudenta(
            @Path("id") id: String
    ): Response<List<Grupa>>

    @GET("student/{id}/grupa")
    suspend fun getGrupaZaStudenta(
        @Path("id") id: String
    ): Response<Grupa>


    @GET("predmet/{id}/grupa")
    suspend fun getGrupeZaPredmet(
            @Path("id") id: Int
    ): Response<List<Grupa>>

    @GET("predmet/{id}/grupa")
     fun getGrupeZaPredmet2(
            @Path("id") id: Int
    ): Call<List<Grupa>>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun dodajStudentaSaHashUGrupuSaId(
            @Path("gid") gid: Int,
            @Path("id") id: String //=AccountRepository.getHash()
    ): Response<MyString>

    @POST("grupa/{gid}/student/{id}")
     fun dodajStudentaSaHashUGrupuSaId2(
            @Path("gid") gid: Int,
            @Path("id") id: String = AccountRepository.acHash
    ): Call<List<Grupa>>
    //kraj grupa
    //kviz
   @GET("kviz")
   suspend fun getKvizovi(): Response<List<Kviz>>

   @GET("kviz/{id}")
   suspend fun getKvizSaId(
           @Path("id") id:Int
   ): Response<Kviz>

   @GET("grupa/{id}/kvizovi")
   suspend fun getKvizoviZaGrupu(
           @Path("id") id: Int
   ): Response<Array<Kviz>>  //array ili list
   //kraj kviz
   //odgovor

   @GET("student/{id}/kviztaken/{ktid}/odgovori")
   suspend fun pokusajKvizStudent(
           @Path("id") id:String,
           @Path("ktid") kdit:Int
   ): Response<List<Odgovor>>

 // @Headers("Content-Type: application/json")
   @POST("student/{id}/kviztaken/{ktid}/odgovor") // ????????????????????????????????????????
   suspend fun dodajOdgovorPokusajKvizStudent(
           @Path("id") id:String,
           @Path("ktid") ktid:Int,
           @Body odgovor: OdgovorPitanje
   ): Response<MyString>//Response<String>


   @Headers("Content-Type: application/json")
    @POST("student/{id}/kviztaken/{ktid}/odgovor") // ????????????????????????????????????????
    suspend fun dodajOdgovorPokusajKvizStudent2(
        @Path("id") id:String,
        @Path("ktid") ktid:Int,
        @Body() odgovor: OdgovorPitanje
    ): Response<Odgovor>
   // kraj odgovor
   //kviz taken

   @GET("student/{id}/kviztaken")
   suspend fun getPokusajiZaKvizoveStudenta(
           @Path("id") id:String
   ): Response<List<KvizTaken>>

    @GET("student/{id}/kviztaken")
    suspend fun getPokusajiZaKvizoveStudenta2(
        @Path("id") id:String
    ): Response<KvizTaken>

   @POST("student/{id}/kviz/{kid}")
   suspend fun zapocniOdgovaranje(
           @Path("id") id:String,
           @Path("kid") kid:Int
   ): Response<KvizTaken>
   //kraj
   //account
   @GET("student/{id}")
   suspend fun getStudentSaHashom(@Path("id") id:String): Response<StudentPodaci>

   @DELETE("student/{id}/upisugrupeipokusaji")
   suspend fun obrisi(@Path("id") id:String): Response<MyString>
   //kraj
   //pitanje
   @GET("kviz/{id}/pitanja")
   suspend fun getPitanjaNaKvizu(@Path("id") id:Int): Response<List<Pitanje>>
    @GET("kviz/{id}/pitanja")
    suspend fun getPitanjaNaKvizu2(@Path("id") id:Int): Response<Pitanje>

}











