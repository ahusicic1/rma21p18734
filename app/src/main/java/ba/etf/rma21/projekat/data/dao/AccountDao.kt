package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import ba.etf.rma21.projekat.data.models.Account

@Dao
interface AccountDao {
    @Query("SELECT * FROM account")
    suspend fun getAll(): List<Account>

    @Query("SELECT count(*) FROM account")
    suspend fun brojKolona(): Int

    @Query("SELECT acHash FROM account LIMIT 1")
    suspend fun getAcHash(): String

    @Query("INSERT INTO account(id, acHash, lastUpdate) VALUES(:id, :hash, :datum )")
    suspend fun postaviAcc(id: Int, hash:String, datum: String)

    @Insert
    suspend fun insertAccount(vararg account: Account)

    @Query("UPDATE account SET acHash=:accountHash WHERE id=0")
    suspend fun updateHash(accountHash: String)

    @Query("UPDATE account SET lastUpdate=:noviLastUpdate WHERE id=0")
    suspend fun updateLastUpdate(noviLastUpdate: String)

    @Query("SELECT lastUpdate FROM account LIMIT 1")
    suspend fun getLastUpdate(): String

}