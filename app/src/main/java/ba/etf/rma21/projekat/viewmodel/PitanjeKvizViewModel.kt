package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import android.widget.Toast
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.*
import ba.etf.rma21.projekat.view.KvizListAdapter
import kotlinx.coroutines.*
import java.lang.NullPointerException

class PitanjeKvizViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)
   companion object{ private var listPitanjaKviz:MutableList<PitanjeKviz> = PitanjeKvizRepository.getSvaPitanjaKviz().toMutableList()
       var listaPredmeta: MutableList<Predmet> = mutableListOf()
   }

fun getPitanja(kvizId:Int,
               onSuccess: (kvizovi: List<Pitanje>) -> Unit,
               onError: () -> Unit){

    scope.launch{
        val datumPocetkaKviza = KvizRepository.getById(kvizId)!!.datumPocetka
        val result =  PitanjeKvizRepository.getPitanja(kvizId)
        when (result) {
            is List<Pitanje> -> onSuccess?.invoke(result)
            else-> onError?.invoke()
        }
    }
}

    fun writePitanje(context: Context, pitanje: Pitanje,  onSuccess:(pitanje: String)->Unit, onError: () -> Unit){
        scope.launch {
            val result = PitanjeKvizRepository.writePitanja(context, pitanje)
            when(result){
                is String -> onSuccess?.invoke(result)
                else->onError?.invoke()
            }
        }
    }


    //

    fun postaviNetacneOdgovore(nazivKviza: String, nazivPredmeta: String){
        //pomocna funkcija za crvene kvizove
        val svaPitanja = PitanjaRepository.getSvaPitanja()
        var trazenoPitanje: Pitanje? = null
        for(i in listPitanjaKviz.indices){
            if(listPitanjaKviz[i].kviz==nazivKviza && listPitanjaKviz[i].predmetNaziv==nazivPredmeta){
                for(j in svaPitanja.indices){
                    if(svaPitanja[j].naziv == listPitanjaKviz[i].naziv){
                        trazenoPitanje = svaPitanja[j]
                        break
                    }
                }
                for(j in trazenoPitanje!!.opcije.indices){
                    if((j+1) != trazenoPitanje.tacan ){

                        listPitanjaKviz[i].odgovor = trazenoPitanje.opcije.get(j) //trazenoPitanje.opcije[j]
                        break
                    }
                }
            }
        }
    }

    fun getPitanjeKviz(nazivKviza: String, nazivPredmeta: String, nazivPitanja: String): PitanjeKviz?{
        var kvizPitanje = PitanjeKvizRepository.getSvaPitanjaKviz().find { it.naziv.equals(nazivPitanja) }  //listPitanjaKviz.find { it.naziv.equals(nazivPitanja) }
        return kvizPitanje
    }

    fun dajPredmet(pitanje: Pitanje, kviz: String): String{
        for(i in listPitanjaKviz.indices){
            if(listPitanjaKviz[i].kviz == kviz && listPitanjaKviz[i].naziv== pitanje.naziv) return listPitanjaKviz[i].predmetNaziv
        }
        return ""
    }

    fun getPitanje(pitanje: String): Pitanje{
        return PitanjaRepository.getSvaPitanja().find { p -> p.naziv==pitanje  }!!
    }

    fun getKvizPitanja( kviz: String, predmet: String): List<PitanjeKviz>{
        return listPitanjaKviz.filter { pitanjeKviz -> pitanjeKviz.predmetNaziv==predmet && pitanjeKviz.kviz==kviz  }
    }

    fun dodajMojOdgovor(pitanje: Pitanje, odgovor: String){
        for(i in listPitanjaKviz.indices){
            if(listPitanjaKviz[i].naziv == pitanje.naziv) listPitanjaKviz[i].odgovor = odgovor
        }
    }

    fun dajMojOdgovorNaPitanje(pitanje: Pitanje): String{
        val odgovorenaPitanja = dajOdgovorenaPitanjaKviz()
        for(i in odgovorenaPitanja.indices){
            if(odgovorenaPitanja[i].naziv == pitanje.naziv) return odgovorenaPitanja[i].odgovor
        }
        return ""
    }

    fun dajIndeksMogOdgovora(pitanje: Pitanje): Int{
        var mojOdg = dajMojOdgovorNaPitanje(pitanje)
        for(i in pitanje.opcije.indices){
            if(pitanje.opcije[i] == mojOdg) return i
        }
        return 0
    }

    fun dajOdgovorenaPitanjaKviz(): List<PitanjeKviz>{
        return listPitanjaKviz.filter { pitanjeKviz -> pitanjeKviz.odgovor != ""  }
    }
    fun dajIndeksTacnogOdgovora(nazivPitanja: String): Int{
        val svaPitanja = PitanjaRepository.getSvaPitanja()
        for(i in svaPitanja.indices){
            if(svaPitanja[i].naziv == nazivPitanja){
                val tacanInd = svaPitanja[i].tacan
                return tacanInd-1
            }
        }
        return 0
    }

    fun dajTacanOdgovor(nazivPitanja: String): String{
        val svaPitanja = PitanjaRepository.getSvaPitanja()
        for(i in svaPitanja.indices){
            if(svaPitanja[i].naziv == nazivPitanja){
                val tacanInd = svaPitanja[i].tacan
                return svaPitanja[i].opcije[tacanInd]
            }
        }
        return ""
    }
    fun dajIndeks(pitanje: Pitanje, mojOdg: String): Int{
        val odgovorenaPitanja = dajOdgovorenaPitanjaKviz()
        for(i in odgovorenaPitanja.indices){
            if(odgovorenaPitanja[i].naziv == pitanje.naziv) return i
        }
        return 0
    }


   suspend fun kolikoTacnihOdgovora(idK: Int, nazivKviza: String, nazivPredmeta: String): Double{
        var brojTacnih = 0
        var ukupno = 0
       try {
           var sviKvizovi = ApiConfig.retrofit.getKvizovi().body()
           var sviPredmeti = ApiConfig.retrofit.getPredmeti().body()
           var nasPredmet = sviPredmeti!!.find { predmet -> predmet.naziv == nazivPredmeta }

           //var grupeZaOvajPredmet = ApiConfig.retrofit.getGrupeZaPredmet(nasPredmet!!.id).body()
//           var idKviza = sviKvizovi!!.find { kviz ->
//               kviz.naziv == nazivKviza &&
//                    listaPredmeta.contains(nasPredmet)
//           //               &&  kviz.kvizoviGrupe!=null &&
////                       kviz.kvizoviGrupe.kvizGrupa.find { kg ->
////                           kg.kvizId == kviz.id &&
////                                   grupeZaOvajPredmet!!.find { g -> g.PredmetId == nasPredmet.id && g.id == kg.grupaId } != null
////                       } != null
//           }!!.id
         var pitanjaNaKvizu = PitanjeKvizRepository.getPitanja(idK)
           if (pitanjaNaKvizu.size==0) return 0.00
           return OdgovorRepository.sracunajBodoveZaKviz(pitanjaNaKvizu).toDouble()
       }catch (e: NullPointerException){
           return 0.00
       }
    }

//    fun kolikoTacnihOdgovora(nazivKviza: String, nazivPredmeta: String): Double{
//       var brojTacnih = 0
//        var ukupno = 0
//        for(i in listPitanjaKviz.indices){
//            if(listPitanjaKviz[i].predmetNaziv == nazivPredmeta && listPitanjaKviz[i].kviz == nazivKviza){
//                if( listPitanjaKviz[i].odgovor == dajTacanOdgovor(listPitanjaKviz[i].naziv ) ){ brojTacnih++ }
//                    ukupno++
//        }
//        }
//        if(ukupno == 0) return 0.00
//        return ( brojTacnih.toDouble()/ukupno.toDouble())
//    }

    fun getPitanjaZaKvizIPostavi(kvizId: Int,
                                 holder: KvizListAdapter.KvizViewHolder,
                                 position: Int,
                                 onSuccess: (pitanjaNaKvizu: List<Pitanje>, holder: KvizListAdapter.KvizViewHolder, position: Int) -> Unit,
                                 onError: () -> Unit){


        scope.launch{
            var result = ApiConfig.retrofit.getPitanjaNaKvizu(kvizId).body()
            when (result) {
                is List<Pitanje> ->{ onSuccess?.invoke(result, holder, position) }
                else-> onError?.invoke()
            }
        }
    }

    fun getPitanja(nazivKviza: String, nazivPredmeta: String): List<Pitanje>{
        val svaKvizPitanja = PitanjeKvizRepository.getSvaPitanjaKviz()
        val svaPitanja = PitanjaRepository.getSvaPitanja()

        var trazenaPitanja: MutableList<Pitanje> = mutableListOf()
        for(i in svaKvizPitanja.indices){
            // prolazimo kroz kvizPitanja: ako se naziv kviza i nazov predmeta
            // slažu sa parametrima, onda je traženo pitanje ono pitanje
            // iz svih pitanja koje ima naziv kao svaKvizPitanja.naziv

            if(svaKvizPitanja[i].kviz == nazivKviza && svaKvizPitanja[i].predmetNaziv == nazivPredmeta){
                var trazenoPitanje = svaPitanja.find { pitanje -> pitanje.naziv == svaKvizPitanja[i].naziv }
                if(trazenoPitanje != null){
                    trazenaPitanja.add(trazenoPitanje)
                }
            }

        }

        return trazenaPitanja.toList()
    }

//    fun getSvaPitanja(): List<Pitanje>{
//        return PitanjaKvizRepository.getSvaPitanja()
//    }
}