package ba.etf.rma21.projekat.data.staticData

import ba.etf.rma21.projekat.data.models.Predmet


fun predmeti(): List<Predmet>{
    return listOf(
        Predmet(1, "IM", 1), Predmet(2, "MLTI", 1), Predmet(3, "OS", 1),
        Predmet(4,"TP", 1), Predmet(5,"DM", 2), Predmet(6,"RMA", 2),
        Predmet(7,"ORM", 2), Predmet(8,"LD", 2), Predmet(9,"OBP", 2),
        Predmet(10,"OOAD", 2), Predmet(11,"DPS", 2), Predmet(12,"IEK", 2),
        Predmet(13,"WT", 3), Predmet(14,"DSU", 3), Predmet(15,"RA", 2)
    )
}

fun pocetniUpisaniPredmeti(): List<Predmet>{

    return listOf(
        Predmet(6,"RMA", 2),
        Predmet(7,"ORM", 2),
        Predmet(15,"RA", 2),
        Predmet(10,"OOAD", 2)
    )
}
