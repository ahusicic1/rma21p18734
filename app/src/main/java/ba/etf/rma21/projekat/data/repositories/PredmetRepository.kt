package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.MyString
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.staticData.pocetniUpisaniPredmeti
import ba.etf.rma21.projekat.data.staticData.predmeti
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.IllegalStateException
import java.lang.NullPointerException

class PredmetRepository {
    companion object {
       private var mojiPredmeti: MutableList<Predmet> = pocetniUpisaniPredmeti()
           .toMutableList()

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }


        suspend fun upisiNaPredmet(predmet: Predmet){
            return withContext(Dispatchers.IO){
                try {
                    var db = AppDatabase.getInstance(context)
                    db.predmetDao().insertAll(predmet)
                    var gid = ApiConfig.retrofit.getGrupeZaPredmet(predmet.id).body()!!.get(0).id
                     ApiConfig.retrofit.dodajStudentaSaHashUGrupuSaId(gid, AccountRepository.getHash())
                    return@withContext
                }catch (e:IllegalStateException){
                    return@withContext
                }
            }
            //mojiPredmeti.add(predmet)
        }

        suspend fun getPredmetsByGodina(godina: Int): List<Predmet>{

            return withContext(Dispatchers.IO){
                try {
                    return@withContext PredmetIGrupaRepository.getPredmeti().filter{ p->p.godina==godina }
                }catch (e:IllegalStateException){
                    return@withContext emptyList<Predmet>()
                }
            }
          //  return getAll().filter { predmet->predmet.godina==godina }
        }

        suspend  fun getUpisani(): List<Predmet> {
            return withContext(Dispatchers.IO){
                try {
//                  var sviPr = PredmetIGrupaRepository.getPredmeti()
//                   var mojeGrupe = PredmetIGrupaRepository.getUpisaneGrupe()
//                   var rez = sviPr.filter{ p -> PredmetIGrupaRepository.getGrupeZaPredmet(p.id)!!.find{ g->mojeGrupe!!.contains(g) }!=null }
                    var db = AppDatabase.getInstance(context)
                    var upisaniPredmeti = db.predmetDao().dajSvePredmete()
                    return@withContext upisaniPredmeti
                }catch (e:NullPointerException){
                    return@withContext emptyList<Predmet>()
                }
            }
        }

        fun setUpisani(predmeti: List<Predmet>){
            mojiPredmeti=predmeti.toMutableList()
        }


       suspend fun getAll(): List<Predmet> {
           return withContext(Dispatchers.IO){
               try {
                   return@withContext PredmetIGrupaRepository.getPredmeti()
               }catch (e:IllegalStateException){
                   return@withContext emptyList<Predmet>()
               }
           }
        }

        suspend fun getNeupisani(): List<Predmet>{
            return withContext(Dispatchers.IO) {
                try {
                    var svi = getAll()
                    return@withContext svi.filter { p-> getUpisani().contains(p)==false }
                } catch (e: IllegalStateException) {
                    return@withContext emptyList<Predmet>()
                }
            }
        }

        suspend fun getNeupisaniZaGodinu(godina: Int): List<Predmet>{
            return withContext(Dispatchers.IO) {
                return@withContext getNeupisani().filter { predmet -> predmet.godina == godina }
            }
        }

    }

}