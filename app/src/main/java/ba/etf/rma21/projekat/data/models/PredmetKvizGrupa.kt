package ba.etf.rma21.projekat.data.models

class PredmetKvizGrupa(
        var predmetId: Int,
        var kvizId: Int,
        var grupaId: Int,
        var nazivPredmeta: String
) {
}