package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.MyString
import ba.etf.rma21.projekat.data.staticData.mojeGrupe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.IllegalStateException
import java.lang.NullPointerException

class GrupaRepository {
    companion object {
        init {
            // TODO: Implementirati
        }
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun dajMojeGrupe(context: Context) : List<Grupa> {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var grupe = db!!.grupaDao().dajMojeGrupe()
                return@withContext grupe
            }
        }

        suspend fun upisiMojeGrupe(context: Context,grupa:Grupa) : String?{
            return withContext(Dispatchers.IO) {
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.grupaDao().insertAll(grupa)
                    var kvizoviZaGrupu = ApiConfig.retrofit.getKvizoviZaGrupu(grupa.id).body()
                    for(i in kvizoviZaGrupu!!.indices) {
                        db.kvizDao().dodajMojKviz(kvizoviZaGrupu[i])
                    }
                    return@withContext "success"
                }
                catch(error:Exception){
                    return@withContext null
                }
            }
        }

        suspend fun upisiUGrupu(id: Int): MyString{
            return withContext(Dispatchers.IO){
                Log.i("grupa repoz", "test")
                try {
                    Log.i("acHash u upisiUGrupu grupaRepoz = ", AccountRepository.getHash())
                    var grupaSaId = ApiConfig.retrofit.getGrupaId(id).body()!!
//                    var db = AppDatabase.getInstance(context)
//                    db.grupaDao().insertAll(grupaSaId)
                    var kvizoviZaGrupu = ApiConfig.retrofit.getKvizoviZaGrupu(id).body()
//                    for(i in kvizoviZaGrupu!!.indices) {
//                        db.kvizDao().dodajMojKviz(kvizoviZaGrupu[i])
//                    }
                    return@withContext ApiConfig.retrofit.dodajStudentaSaHashUGrupuSaId(id, AccountRepository.getHash()).body()!!

                }catch (e:IllegalStateException){
                    return@withContext MyString("")
                }
            }
        }

        private var mojeGrupe: MutableList<Grupa> = mojeGrupe().toMutableList()


        suspend fun getGroupsByPredmet(nazivPredmeta: String): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                try {
                    var predmeti = ApiConfig.retrofit.getPredmeti().body()
                    var trazeniPredmet = predmeti?.find { predmet -> predmet.naziv == nazivPredmeta }
                    return@withContext ApiConfig.retrofit.getGrupeZaPredmet(trazeniPredmet!!.id).body()
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Grupa>()
                }catch (e: NullPointerException){
                    return@withContext emptyList<Grupa>()
                }
            }
         //  return grupe().filter { grupa -> grupa.nazivPredmeta==nazivPredmeta }
        }

        suspend fun getMojeGrupe(): List<Grupa>{
            return withContext(Dispatchers.IO) {
                var account = ApiConfig.retrofit.getStudentSaHashom(AccountRepository.getHash())
                var lista: MutableList<Grupa> = mutableListOf()
                if(account.body() == null) return@withContext lista

                try {
                    var rez = ApiConfig.retrofit.getGrupeZaStudenta(AccountRepository.getHash()).body()!!
                    for(i in rez.indices) lista.add(rez[i])
                    //if (ApiConfig.retrofit.getGrupeZaStudenta(account.body()!!.id).isSuccessful == false) return@withContext emptyList<Grupa>()
                }catch (e: IllegalStateException){
                    var grupa: Grupa? = ApiConfig.retrofit.getGrupaZaStudenta(AccountRepository.getHash()).body()

                    if(grupa == null) return@withContext lista
                    lista.add(grupa!!)

                    return@withContext lista
                }
                return@withContext lista
            }
          // return  mojeGrupe.toList()
        }
        suspend fun upisiUGrupu(grupa: Grupa): Boolean?{
            var poruka = GrupaRepository.upisiUGrupu(grupa.id).toString()
            //    return@withContext
            return poruka.contains("Student ahusicic1@etf.unsa.ba je dodan u grupu")
           // mojeGrupe.add(grupa)
        }
//
//        fun setMojeGrupe(grupe: List<Grupa>){
//            mojeGrupe=grupe.toMutableList()
//        }

    }
}