package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PredmetViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun writePredmet(context: Context, predmet: Predmet,
                                          onSuccess: (predmet: String) -> Unit,
                                          onError: () -> Unit){
        scope.launch {
            var result = PredmetIGrupaRepository.writePredmet(context, predmet)
            when (result) {
                is String -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
        // PredmetRepository.upisiNaPredmet(predmet)
    }

    fun getPredmeti(onSuccess: (predmeti: List<Predmet>) -> Unit,
                 onError: () -> Unit){

        // Create a new coroutine on the UI thread
        scope.launch{

            // Make the network call and suspend execution until it finishes
            val result = PredmetIGrupaRepository.getPredmeti()

            // Display result of the network request to the user
            when (result) {
                is List<Predmet> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

     fun upisiNaPredmet(predmet: Predmet,
                                onSuccess: (grupe: List<Grupa>) -> Unit,
                                onError: () -> Unit){
        scope.launch {
            var grupe = PredmetIGrupaRepository.getGrupeZaPredmet(predmet.id)
           // if (grupe == null || grupe.size == 0) return
            if(grupe!= null && grupe!!.size != 0 ) PredmetIGrupaRepository.upisiUGrupu(grupe!![0].id)
            when (grupe) {
                is List<Grupa> -> onSuccess?.invoke(grupe)
                else-> onError?.invoke()
            }
        }
       // PredmetRepository.upisiNaPredmet(predmet)
    }

     suspend  fun getNeupisani(): List<Predmet>{

        var upisaneGrupe = PredmetIGrupaRepository.getUpisaneGrupe()
        var upisaniPredmeti: MutableList<Int> = mutableListOf()
         upisaneGrupe!!.forEach { g -> upisaniPredmeti.add(g.PredmetId) }
        var sviPredmeti = PredmetIGrupaRepository.getPredmeti()
       return   sviPredmeti.filter { p-> !upisaniPredmeti.contains(p.id) }
//            when (sviPredmeti) {
//                is List<Predmet> -> onSuccess?.invoke(sviPredmeti)
//                else-> onError?.invoke()
//            }

    }

    suspend fun getNeupisaniZaGodinu(godina: Int): List<Predmet>{
         return getNeupisani().filter { p -> p.godina == godina } //PredmetRepository.getNeupisaniZaGodinu(godina)
    }


}