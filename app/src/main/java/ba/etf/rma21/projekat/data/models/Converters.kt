package ba.etf.rma21.projekat.data.models

import androidx.room.TypeConverter
import androidx.room.TypeConverters
import java.text.SimpleDateFormat
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimeStamp(value: String?): Date?{
        if(value!=null){
            var tmp = value.replace("T", ".")
            val dateTimeFormatGmt = SimpleDateFormat("yyyy-MM-dd.HH:mm:ss")
            dateTimeFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"))
            return dateTimeFormatGmt.parse(tmp)
        }
        return null
    }

    @TypeConverter
    fun dateToTimeStamp(date: Date?): String?{
        if(date!=null){
            val dateTimeFormatGmt = SimpleDateFormat("yyyy-MM-dd.HH:mm:ss")
            dateTimeFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT+2"))
            var tmp = dateTimeFormatGmt.format(date)
            tmp = tmp.replace(".", "T")
            return tmp
        }
        return null
    }

//    @TypeConverter
//    fun stringToDate(value: String): Date?{
//        if(value==null) return null
//        val dat = value.split('T')[0]
//        val datumi = dat.split('-').map { d->d.toInt() }
//        return Date(datumi[0]-1900, datumi[1], datumi[2])
//    }
//
//    @TypeConverter
//    fun dateToString(date: Date?):String?{
//        if(date==null) return null
//        return date.toString()
//    }

    @TypeConverter
    fun fromString(s: String): List<String>{
        return s.split(",").map{it}
    }

    @TypeConverter
    fun toString(s: List<String>): String{
        return s.joinToString (separator=",")
    }

}