package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface OdgovorDao {
    @Query("SELECT * FROM odgovor")
    suspend fun getAll(): List<Odgovor>

    @Query("SELECT id FROM odgovor WHERE idPitanje=:idPitanja AND idKviz=:idKviza LIMIT 1")
    suspend fun getOdgovorId(idPitanja: Int, idKviza: Int): Int

    @Query("UPDATE odgovor SET odgovoreno=:odgovor WHERE id=:idOdgovora")
    suspend fun postaviOdgovor(idOdgovora: Int, odgovor: Int)

    @Query("INSERT INTO odgovor(id, odgovoreno, idPitanje, idKviz, KvizTakenId) VALUES(:sljedeciId, :odgovor, :idPitanje, :kvizId, :idKvizTaken)")
    suspend fun dodajOdgovor(sljedeciId: Int, odgovor: Int, idPitanje: Int, kvizId: Int, idKvizTaken: Int)

    @Insert
    suspend fun insertAll(vararg odgovor: Odgovor)

    @Query("DELETE FROM kviz")
    suspend fun obrisi()

}