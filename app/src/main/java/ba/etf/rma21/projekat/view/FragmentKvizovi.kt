package ba.etf.rma21.projekat.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizResponse
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import kotlinx.coroutines.*

class FragmentKvizovi: Fragment() {
        private lateinit var listaKvizova: RecyclerView
        private lateinit var filterKvizova: Spinner
        private var listaKvizovaAdapter: KvizListAdapter? = null
        private var kvizListViewModel = KvizListViewModel()

    fun onSuccess(kvizovi:List<Kviz>){
        val toast = Toast.makeText(context, "kvizovi.size = ${kvizovi.size}", Toast.LENGTH_SHORT)
         toast.show()
      listaKvizovaAdapter?.updateKvizovi(kvizovi)
    }
    fun onSuccess2(kvizovi:List<Kviz>){
        GlobalScope.launch(Dispatchers.IO){ withContext(Dispatchers.Main){
            listaKvizovaAdapter!!.updateKvizovi(kvizovi)
        } }
    }

    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.kviz_fragment, container, false)
        listaKvizova = view.findViewById(R.id.listaKvizova)

        listaKvizova.layoutManager = GridLayoutManager(activity, 2)
        listaKvizovaAdapter = KvizListAdapter(activity!!, listOf())
        listaKvizova.adapter = listaKvizovaAdapter

        kvizListViewModel.getMyKvizes(onSuccess = ::onSuccess2, onError = ::onError)

        val spinner: Spinner = view.findViewById(R.id.filterKvizova)
        ArrayAdapter.createFromResource(
                activity!!,
                R.array.filtriranje,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }

        spinner.onItemSelectedListener = object :

                AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {

                when (position) {
                    0 -> kvizListViewModel.getMyKvizes(onSuccess = ::onSuccess2, onError = ::onError)//listaKvizovaAdapter!!.updateKvizovi(kvizListViewModel.getMyKvizes())
                    1 ->  kvizListViewModel.getAll(onSuccess = ::onSuccess2, onError = ::onError)
                    2 -> kvizListViewModel.getDone(onSuccess = ::onSuccess2, onError = ::onError)
                    3 -> kvizListViewModel.getFuture(onSuccess = ::onSuccess2, onError = ::onError)
                    4 -> kvizListViewModel.getNotTaken(onSuccess = ::onSuccess2, onError = ::onError)
                    else -> kvizListViewModel.getMyKvizes(onSuccess = ::onSuccess2, onError = ::onError)
                }

            }
        }

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                (activity as MainActivity).supportFragmentManager.beginTransaction().replace(R.id.container, FragmentKvizovi.newInstance()).addToBackStack(null).commit()
                (activity as MainActivity).odaberiOpcijuBottomNav(R.id.kvizovi)
            }
        })



        return view
    }


    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }

     fun showKvizDetails(kviz: Kviz){
        val intent = Intent(activity, MainActivity::class.java).apply {
            putExtra("kviz_naziv", kviz.naziv)
        }
        startActivity(intent)
    }

    fun getAdapter(): KvizListAdapter?{
        return listaKvizovaAdapter
   }


}