package ba.etf.rma21.projekat.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.UpisPredmet
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.*
//import ba.etf.rma21.projekat.data.repositories.GrupaRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class FragmentPredmeti: Fragment() {
    private lateinit var odabirGodina: Spinner
    private lateinit var odabirPredmet: Spinner
    private lateinit var odabirGrupa: Spinner
    private lateinit var upisaniPredmeti: String
    private lateinit var upisaniPredmet: Predmet
    private lateinit var upisanaGrupa: Grupa
    private lateinit var upisaniKvizovi: List<Kviz>
    private lateinit var dodajPredmetDugme: Button
    private var kvizListViewModel = KvizListViewModel()
    private var predmetViewModel = PredmetViewModel()
    private var grupaViewModel = GrupaViewModel()
    var odabraniPredmet: Predmet? = Predmet(0, "", 0)
    var odabranaGrupa: Grupa? = Grupa(0, "", 0)
    private lateinit var listaKvizovaAdapter: KvizListAdapter // TODO
    val scope = CoroutineScope(Job() + Dispatchers.Main)
    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
        var zadnjaGodina: Int = 0
         var pamtiGodinu = 0
        var pamtiPredmet = 0
        var pamtiGrupu = 0
//        var backGodina: Boolean = false
//        var backPredmet: Boolean = false
//        var backGrupa: Boolean = false
    }

    override fun onPause() {
        super.onPause()
        pamtiGodinu = odabirGodina.selectedItemPosition
        pamtiPredmet = odabirPredmet.selectedItemPosition
        pamtiGrupu = odabirGrupa.selectedItemPosition
//        backGodina = true
//        backPredmet = true
//        backGrupa = true
    }

    override fun onResume() {
        super.onResume()
        odabirGodina.setSelection(pamtiGodinu, false)
        odabirPredmet.setSelection(pamtiPredmet, false)
        odabirGrupa.setSelection(pamtiGrupu, false)
    }

    fun restartuj(){
        pamtiGodinu=0
        pamtiGrupu=0
        pamtiPredmet=0
    }

    fun writeGrupa(){
        grupaViewModel.writeGrupa(this.requireContext(), upisanaGrupa, onSuccess = ::onSuccessMsg, onError = ::onError)
    }
    fun writePredmet(){
        predmetViewModel.writePredmet(this.requireContext(), upisaniPredmet, onSuccess = ::onSuccessMsg, onError = ::onError)
    }
    fun writeKvizovi(){
        Log.i("kviz", "fragment predmeti writeKvizovi")
        kvizListViewModel.writeKvizovi(this.requireContext(), upisaniPredmet, upisanaGrupa, onSuccess=::onSuccessMsg, onError=::onError)
    }

    fun onSuccessMsg(msg: String){}

    fun onSuccess(grupe: Array<*>){
        val grupaAdapter = ArrayAdapter(
                activity!!, // //TODO popraviti !!
                android.R.layout.simple_spinner_item,
                grupe
        )
        grupaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        odabirGrupa.adapter = grupaAdapter

        odabirGrupa.setSelection(pamtiGrupu, false)
    }
    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }

    fun onSuccessWriteWrite(tmp: Boolean){
        writePredmet()
        writeGrupa()
        writeKvizovi()
    }
    fun onSuccess2(b: Boolean){}
    fun onSuccess3(grupe: List<Grupa>){}
    fun onSuccessGr(grupe: List<Grupa>){
        Log.i("onSuccessGr", "on success treba pozvat upisiUGr iz grupaVM")
        for(g in grupe){
            Log.i("onSuccessGr", "g.naziv="+g.naziv+"odabranaGrupa.naziv="+odabranaGrupa?.naziv+"odabirGrupa.selectedNaziv="+odabirGrupa.selectedItem.toString())
            if(g.naziv == odabirGrupa.selectedItem.toString()){
                upisanaGrupa = g
                grupaViewModel.upisiUGrupu(g, onSuccess = ::onSuccessWriteWrite, onError = ::onError)
            }
        }
    }
    fun onSuccessPr(predmeti: List<Predmet>){
        for(g in predmeti){
            if(g.naziv == odabirPredmet.selectedItem.toString()){
                upisaniPredmet = g
            }
        }
    }
    fun onSuccessKv(kvizovi: List<Kviz>){
           Log.i("kviz", "fragment predmeti onSuccessKv")
    }


    fun onSuccessNeupisaniPredmeti(neupisani: List<Predmet>) {
        try {
            odabraniPredmet = neupisani.find { p -> p.naziv == odabirPredmet.selectedItem.toString() }
            var grupeLista: List<Grupa> = listOf()
            grupeLista?.let { grupaViewModel.getGroupsByPredmet(odabraniPredmet!!.id, onSuccess = ::onSuccess, onError = ::onError) }
            odabranaGrupa = grupeLista?.find { grupa -> grupa.naziv == odabirGrupa.selectedItem.toString() }
        }catch (e: NullPointerException){
            return
        }
//
    }
    fun postaviAdapter(stringPredmeti: Array<*>){
        val predmetAdapter = ArrayAdapter(
                activity!!,   //TODO popraviti !!
                android.R.layout.simple_spinner_item,
                stringPredmeti
                    )
                    predmetAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    odabirPredmet.adapter = predmetAdapter

                  odabirPredmet.setSelection(pamtiPredmet, false)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.predmeti_fragment, container, false)
        odabirGodina = view.findViewById(R.id.odabirGodina)
        odabirPredmet = view.findViewById(R.id.odabirPredmet)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)
        dodajPredmetDugme = view.findViewById(R.id.dodajPredmetDugme)

        listaKvizovaAdapter = KvizListAdapter(activity!!, listOf()) //TODO popraviti !!
        dodajPredmetDugme.isEnabled=true


        dodajPredmetDugme.setOnClickListener {
//            scope.launch{
//                var neupisani=predmetViewModel.getNeupisaniZaGodinu(odabirGodina.selectedItem.toString().toInt())
//                when (neupisani) {
//                    is List<Predmet>-> onSuccessNeupisaniPredmeti(neupisani)
//                    else-> onError()
//                }
//            }
//            predmetViewModel.upisiNaPredmet(odabraniPredmet!!, onSuccess = ::onSuccess3, onError = ::onError)
//
//            grupaViewModel.upisiUGrupu(odabranaGrupa!!, onSuccess = ::onSuccess2, onError = ::onError)
//            kvizListViewModel.dodajKvizoveZaPredmet(odabraniPredmet!!, odabranaGrupa!!.naziv)

            grupaViewModel.getGrupe(onSuccess = ::onSuccessGr, onError = ::onError)
            predmetViewModel.getPredmeti(onSuccess = ::onSuccessPr, onError = ::onError)
            kvizListViewModel.getKvizovi(onSuccess = ::onSuccessKv, onError = ::onError)

            UpisPredmet.zadnjaGodina = odabirGodina.selectedItemPosition
            val bundle = Bundle()
            if(odabirPredmet.selectedItem!=null)   bundle.putString("predmet", odabirPredmet.selectedItem.toString())
            if(odabirGrupa.selectedItem!=null)     bundle.putString("grupa", odabirGrupa.selectedItem.toString())
            bundle.putString("poruka", "")

            val newFragment = FragmentPoruka.newInstance()
            newFragment.arguments = bundle

           replaceFragment(newFragment)
        }

            ArrayAdapter.createFromResource(
                    activity!!,  //TODO popraviti !!
                    R.array.godina,
                    android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                odabirGodina.adapter = adapter

                odabirGodina.setSelection(UpisPredmet.zadnjaGodina)
            }



            odabirGodina.onItemSelectedListener = object :

                    AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    // dodajPredmetDugme.isEnabled=false
                }

                override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                ) {

                    scope.launch {
                        var stringPredmeti = PredmetRepository.getNeupisaniZaGodinu(
                            odabirGodina.selectedItem.toString().toInt()
                        ).stream().map { it.naziv }.toArray()
                        when (stringPredmeti) {
                            is Array<*> -> postaviAdapter(stringPredmeti)
                            else -> onError()
                        }
                    }
                }

            } //kraj


            odabirPredmet.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    dodajPredmetDugme.isEnabled=false
                }

                override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int ,
                        id: Long
                ) {
                    scope.launch{
                        var grupe: Array<*>? = null
                       if(odabirPredmet.selectedItem!=null){
                            grupe = GrupaRepository.getGroupsByPredmet(odabirPredmet.selectedItem.toString())!!.stream().map { it.naziv }.toArray()
                           dodajPredmetDugme.isEnabled = !grupe!!.isEmpty()
                       }
                                       // .stream().map { it.naziv }.toArray()
                        // Display result of the network request to the user
                        when (grupe) {
                            is Array<*> -> onSuccess(grupe)
                            else-> onError()
                        }
                    }

//                    dodajPredmetDugme.isEnabled = !grupe.isEmpty()
//
//                    val grupaAdapter = ArrayAdapter(
//                            activity!!, // //TODO popraviti !!
//                            android.R.layout.simple_spinner_item,
//                            grupe
//                    )
//                    grupaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                    odabirGrupa.adapter = grupaAdapter
//
//                    odabirGrupa.setSelection(pamtiGrupu, false)
                }

            }

            odabirGrupa.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }

                override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                ) {
                    dodajPredmetDugme.isEnabled = odabirGrupa.selectedItem != null

                }



        }


        /**********************************************************/
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                (activity as MainActivity).supportFragmentManager.beginTransaction().replace(R.id.container, FragmentKvizovi.newInstance()).addToBackStack(null).commit()
                (activity as MainActivity).odaberiOpcijuBottomNav(R.id.kvizovi)
            }
        })


        return view
        }

    fun replaceFragment(someFragment: Fragment? ) {
        val transaction = fragmentManager!!.beginTransaction()
        transaction.replace(R.id.container, someFragment!!)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    private fun showKvizDetails(kviz: KvizResponse){
        val intent = Intent(activity, MainActivity::class.java).apply {
            putExtra("kviz_naziv", kviz.naziv)
        }
        startActivity(intent)
    }

}

