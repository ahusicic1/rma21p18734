package ba.etf.rma21.projekat

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.repositories.*
import ba.etf.rma21.projekat.view.FragmentKvizovi
import ba.etf.rma21.projekat.view.FragmentPoruka
import ba.etf.rma21.projekat.view.FragmentPredmeti
import ba.etf.rma21.projekat.viewmodel.AccountViewModel
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.OdgovorViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNav: BottomNavigationView
    private lateinit var MyFragment: Fragment
    private var fragmentKvizovi = FragmentKvizovi.newInstance()
    private var fragmentPredmeti = FragmentPredmeti.newInstance()
    private var fragmentPoruka = FragmentPoruka.newInstance()
    private var pitanjeKvizVM = PitanjeKvizViewModel()
    private var kvizListViewModel = KvizListViewModel()
    private var accountVM = AccountViewModel()
    private var odgovorVM = OdgovorViewModel()
    val scope = CoroutineScope(Job() + Dispatchers.Main)
    companion object{ var dodanKviz = 0 }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.kvizovi -> {
                if( fragmentKvizovi.isAdded /*|| dodanKviz==1*/ ){
                   // dodanKviz=0
                   // fragmentPredmeti.restartuj()
                    return@OnNavigationItemSelectedListener true
                }
                fragmentPredmeti.restartuj()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.container, fragmentKvizovi)
                transaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                // val fragmentPredmeti = FragmentPredmeti.newInstance()
                openFragment(fragmentPredmeti)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predajKviz -> {
                var testKvizovi = fragmentKvizovi.getAdapter()!!.getKvizovi()
              //  fragmentKvizovi.showKvizDetails(testKvizovi[0])
              if(fragmentKvizovi.getAdapter()!=null){
                  kvizListViewModel.setContext(this)
                  kvizListViewModel.postaviDatumRada(Calendar.getInstance().time, fragmentKvizovi.getAdapter()!!.getNazivKviza(), fragmentKvizovi.getAdapter()!!.getNazivPredmet())
                  odgovorVM.predajOdgovore(fragmentKvizovi.getAdapter()!!.getIdKviza())
              }
               item.isVisible = false
                bottomNav.menu.findItem(R.id.kvizovi).isVisible = true
                bottomNav.menu.findItem(R.id.predmeti).isVisible = true
                bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = false

                var navigacija = findViewById<NavigationView>(R.id.navigacijaPitanja)
                navigacija.menu.add(R.id.groupId, navigacija.menu.size(), navigacija.menu.size(), "Rezultat")
                navigacija.invalidate()
                ////////////

/////////////////////
                val bundle = Bundle()
               if(fragmentKvizovi.getAdapter() != null) {
                   bundle.putString("nazivKviza", fragmentKvizovi.getAdapter()!!.getNazivKviza())
//                   var procenat = pitanjeKvizVM.kolikoTacnihOdgovora(fragmentKvizovi.getAdapter()!!.getNazivKviza(), fragmentKvizovi.getAdapter()!!.getNazivPredmet())
//                   bundle.putString("procenatTacnost", procenat.toString())

                   scope.launch{
                       var procenat = pitanjeKvizVM.kolikoTacnihOdgovora(fragmentKvizovi.getAdapter()!!.getIdKviza(), fragmentKvizovi.getAdapter()!!.getNazivKviza(), fragmentKvizovi.getAdapter()!!.getNazivPredmet())
                       when (procenat) {
                           is Double -> posaljiProcenat(bundle, procenat)
                           else-> onError()
                       }
                   }
               }
                else {
                   bundle.putString("nazivKviza", "kviz")
                   bundle.putString("procenatTacnost", "")
                   //}
                   val newFragment = FragmentPoruka.newInstance()
                   newFragment.arguments = bundle

                   val transaction = supportFragmentManager.beginTransaction()

                   var test = findViewById<FrameLayout>(R.id.framePitanje)
                   test.removeAllViews()

                   transaction.replace(R.id.framePitanje, newFragment)
                   //  transaction.addToBackStack(null)
                   transaction.commit()
               }
                return@OnNavigationItemSelectedListener true
            }
            R.id.zaustaviKviz -> {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.container, fragmentKvizovi)
                //transaction.addToBackStack(null)
                transaction.commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNav =findViewById(R.id.bottomNav)
        bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)


        bottomNav.menu.findItem(R.id.kvizovi).isVisible = true
        bottomNav.menu.findItem(R.id.predmeti).isVisible = true
        bottomNav.menu.findItem(R.id.predajKviz).isVisible = false
        bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = false
        bottomNav.selectedItemId = R.id.kvizovi

       // val payload = intent?.getStringExtra("payload")
        var toast = Toast.makeText(this, "onCreate " , Toast.LENGTH_SHORT)
        toast.show()

        val uri = intent
        AccountRepository.setContext(this)
        TakeKvizRepository.setContext(this)
        KvizRepository.setContext(this)
        GrupaRepository.setContext(this)
        PredmetIGrupaRepository.setContext(this)
        PredmetRepository.setContext(this)
        PitanjaRepository.setContext(this)
        OdgovorRepository.setContext(this)
        DBRepository.setContext(this)


        if (uri != null) { Log.i("imaLiUri", "imaaa, postavlja se hash")
            val hash = uri.getStringExtra("payload")
            if (hash != null) {
                this?.let { accountVM.postaviHash(it, hash, onSuccess = ::onSuccess, onError = ::onError) }
            }
            else{
                //this?.let{accountVM.postaviHash(it, AccountRepository.acHash, onSuccess = ::onSuccess, onError = ::onError)}
                val formatter = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
                var db = AppDatabase.getInstance(this)
                var accountDao = db.accountDao()
                this?.let{accountVM.postaviHashIDatum(it, AccountRepository.acHash, onSuccess = ::onSuccess, onError = ::onError ) }
            }

        }
        else { Log.i("imaLiUri", "nemaaa, acHash se stavlja")
            this?.let { accountVM.postaviHash(it, AccountRepository.acHash, onSuccess = ::onSuccess, onError = ::onError)}
            val toast = Toast.makeText(this, "Trenutni hash korisnika: " + AccountRepository.acHash, Toast.LENGTH_SHORT)
            toast.show()
        }

       // AccountRepository.postaviHash(payload!!)
        val kvizoviFragment = FragmentKvizovi.newInstance()
        openFragment(kvizoviFragment)
    }


    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        //if(fragment.isAdded) transaction.remove(fragment)
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }


    fun odaberiOpcijuBottomNav(id: Int){
        bottomNav.selectedItemId = id
    }

     fun getBottomNav(): BottomNavigationView{
         return bottomNav
     }

    fun posaljiProcenat(bundle: Bundle, procenat: Double){
        bundle.putString("nazivKviza", fragmentKvizovi.getAdapter()!!.getNazivKviza())
        bundle.putString("procenatTacnost", procenat.toString())
        //
        val newFragment = FragmentPoruka.newInstance()
        newFragment.arguments = bundle

        val transaction = supportFragmentManager.beginTransaction()

        var test = findViewById<FrameLayout>(R.id.framePitanje)
        test?.removeAllViews()

        transaction.replace(R.id.framePitanje, newFragment)
        //  transaction.addToBackStack(null)
        transaction.commit()

    }
    fun onSuccess() {
        val toast = Toast.makeText(this, "Success main", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun onError() {
        val toast = Toast.makeText(this, "Procenat tacnosti error", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun getContext(): Context {
        return this.getContext()
    }


}

