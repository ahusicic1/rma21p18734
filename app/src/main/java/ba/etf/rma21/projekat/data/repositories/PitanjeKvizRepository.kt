package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.PitanjeResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ba.etf.rma21.projekat.data.models.PitanjeKviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.staticData.pitanja
import ba.etf.rma21.projekat.data.staticData.pitanjaKviz
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import java.lang.IllegalStateException

class PitanjeKvizRepository {
    companion object {
        // TODO: Implementirati
        init {
            // TODO: Implementirati
        }

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getPitanja(idKviza: Int): List<Pitanje> {
            return withContext(Dispatchers.IO){
                try {
                    var response = ApiConfig.retrofit.getPitanjaNaKvizu(idKviza).body()
                    return@withContext response!!
                }catch (e: IllegalStateException){ return@withContext emptyList<Pitanje>()}
            }
        }
        suspend fun writePitanja(context: Context, pitanje: Pitanje): String?{
            return withContext(Dispatchers.IO){
                try{
                    var db = AppDatabase.getInstance(context)
//                    pitanje.id = db.pitanjeDao().dajSvaPitanja().size + 10
                    db.pitanjeDao().insertAllPitanje(pitanje)
                    return@withContext "success"
                }catch (e:IllegalStateException){
                    return@withContext null
                }
            }
        }


//
        fun getSvaPitanjaKviz(): List<PitanjeKviz>{
            return   pitanjaKviz()
          }

    }
}