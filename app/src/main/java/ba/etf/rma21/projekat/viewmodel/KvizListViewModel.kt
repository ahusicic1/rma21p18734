package ba.etf.rma21.projekat.viewmodel

import android.app.Application
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.ApiConfig
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.view.KvizListAdapter
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

class KvizListViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)
    private lateinit var context:Context
    fun setContext(_context: Context){
        context=_context
    }
    companion object{ var pocetiKvizovi: MutableList<Kviz> = mutableListOf() }

     fun jeLiCrvenKviz(bundle: Bundle,
                       kviz: Kviz,
                       onSuccess: (bundle: Bundle, crven: Boolean) -> Unit,
                       onError: () -> Unit){
         scope.launch {
             var result = getMyKvizes2().contains(kviz)
             when (result) {
                 is Boolean -> onSuccess?.invoke(bundle, result)
                 else-> onError?.invoke()
             }
         }
    }

    fun writeKvizovi(context: Context, predmet: Predmet, grupa: Grupa, onSuccess: (kviz: String) -> Unit,
                   onError: () -> Unit){
        Log.i("kviz", "kvizListVM writeKvizovi")
        scope.launch{
            var kvizovi = ApiConfig.retrofit.getKvizoviZaGrupu(grupa.id).body()!!.toList()
            Log.i("upisuju se", kvizovi.toString())
            val result = KvizRepository.writeKvizovi(context,kvizovi)
            when (result) {
                is String -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

    fun getKvizovi(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                 onError: () -> Unit){
        scope.launch{
            val result = KvizRepository.getKvizovi()
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }


    fun dajPredmetZaKviz(kviz: Kviz,
                         holder: KvizListAdapter.KvizViewHolder,
                         onSuccess: (predmetiList: List<Predmet>, holder: KvizListAdapter.KvizViewHolder) -> Unit,
                         onError: () -> Unit){
        scope.launch{
            var result = ApiConfig.retrofit.getGrupeZaKviz(kviz.id).body()
            var setPredmeti: MutableSet<Int> = mutableSetOf()
            var listaPredmeta = mutableListOf<Predmet>()

            result!!.forEach { grupa -> setPredmeti.add(grupa.PredmetId) }
            setPredmeti.forEach { predmetId ->
                listaPredmeta.add(
                    ApiConfig.retrofit.getPredmetId(
                        predmetId
                    ).body()!!
                ) && PitanjeKvizViewModel.listaPredmeta.add(
                        ApiConfig.retrofit.getPredmetId(
                                predmetId
                        ).body()!!
                )
            }
            when (result) {
                is List<Grupa> -> onSuccess?.invoke(listaPredmeta, holder)
                else-> onError?.invoke()
            }
        }
    }

    fun postaviDatumRada(date: Date, kviz: String, predmet: String){
       scope.launch {
           var db = AppDatabase.getInstance(context)
           var kvizDao = db.kvizDao()
           var mojiKvizovi = kvizDao.dajSveMojeKvizove()
           for( i in mojiKvizovi.indices){
               if(mojiKvizovi[i].naziv == kviz && mojiKvizovi[i].naziv == predmet){
                   val formatter = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
                kvizDao.postaviDatumRada(mojiKvizovi[i].id, formatter.format(date))
            }
        }
           when(mojiKvizovi){
                is List<Kviz> -> onSuccessPostaviDatum(mojiKvizovi, date, kviz, predmet)
               else -> onError()
           }
       }
    }

    fun dodajMojKviz(kviz: KvizResponse){
       //   KvizRepository.dodajMojKviz(kviz)
    }

    fun dodajKvizoveZaPredmet(predmet: Predmet, nazivGrupe: String){
//       var sviKvizovi = KvizRepository.getAll()
//       for(i in sviKvizovi.indices){
//            if(sviKvizovi[i].nazivPredmeta == predmet.naziv && sviKvizovi[i].nazivGrupe==nazivGrupe) dodajMojKviz(sviKvizovi[i])
//        }
    }

    fun getAll(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                 onError: () -> Unit){

        // Create a new coroutine on the UI thread
        scope.launch{

            val result =  KvizRepository.getAll().sortedBy { it.datumPocetka }

            // Display result of the network request to the user
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }
//    fun getAll(): List<Kviz>{
//        var result: List<Kviz>? = runBlocking{
//            KvizRepository.getAll().sortedBy { it.datumPocetka }
//        }
//        return result!!
//       // return KvizRepository.getAll().sortedBy { it.datumPocetka }
//    }

    fun getMyKvizes(onSuccess: (kvizovi: List<Kviz>) -> Unit,
               onError: () -> Unit){

        scope.launch{

            val result = KvizRepository.getMyKvizes().sortedBy { it.datumPocetka }  //ovdje bila greska
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }
     suspend fun getMyKvizes2(): List<Kviz>{
            return KvizRepository.getMyKvizes().sortedBy { it.datumPocetka }
    }

fun getDone(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                onError: () -> Unit){

    scope.launch{
        val result =  KvizRepository.getDone().sortedBy { it.datumPocetka }
        when (result) {
            is List<Kviz> -> onSuccess?.invoke(result)
            else-> onError?.invoke()
        }
    }
}
    fun getFuture(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                    onError: () -> Unit){

        scope.launch{
            val result =  KvizRepository.getFuture().sortedBy { it.datumPocetka }
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }
    fun getActive(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                    onError: () -> Unit){

        scope.launch{
            val result =  KvizRepository.getActive().sortedBy { it.datumPocetka }
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

    fun getNotTaken(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                  onError: () -> Unit){

        scope.launch{
            val result =  KvizRepository.getNotTaken().sortedBy { it.datumPocetka }
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

    fun onSuccessPostaviDatum(mojiKvizovi: List<Kviz>, date: Date, kviz: String, predmet: String) {

    }
    fun onError() {
        val toast = Toast.makeText(context, "Postavi datum error", Toast.LENGTH_SHORT)
        toast.show()
    }

}