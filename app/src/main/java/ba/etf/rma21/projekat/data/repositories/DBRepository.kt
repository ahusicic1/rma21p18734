package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import org.json.JSONObject
import java.lang.NullPointerException
import java.text.SimpleDateFormat
import java.util.*

class DBRepository {
    companion object {
        private lateinit var context: Context
        var acHash: String = "e75f79ff-ddea-4a2a-a734-9c5e314dbd9b"
        fun setContext(_context: Context) {
            context = _context
        }

        suspend fun updateNow(): Boolean {
            var db = AppDatabase.getInstance(context)
            var account = db.accountDao()
            val format = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")

            var datumZadnjePromjene = account.getLastUpdate()

            Log.i("accHashVal", AccountRepository.getHash());
            var promjeneMsg = ApiConfig.retrofit.imaLiPromjena(acHash, datumZadnjePromjene).body()
            val promjene = promjeneMsg!!.changed

            Log.i("datum zadnje promjene + changed ", (datumZadnjePromjene) + " " + promjene)
            if (promjene == true) {
                AzurirajGrupe()
                AzurirajPredmete()
                AzurirajKvizove()
                AzurirajPitanja()
                AzurirajOdgovore()

                var stringDatum = format.format(Calendar.getInstance().time)
                account.updateLastUpdate(stringDatum)
            }

            Log.i("promjene finalno", "___" + promjene.toString() + "___");
            Log.i("vraca se", promjene.toString());
            return promjene
        }

        suspend fun AzurirajOdgovore(){
            var db = AppDatabase.getInstance(context)
            var odgovorDao = db.odgovorDao()
            odgovorDao.obrisi()
        }

        suspend fun AzurirajGrupe() {
            var noveGrupe =
                ApiConfig.retrofit.getGrupeZaStudenta(AccountRepository.getHash()).body()
            var db = AppDatabase.getInstance(context)
            var grupaDao = db.grupaDao()
            grupaDao.obrisi()
//            for (i in noveGrupe!!.indices) {
//                grupaDao.insertAll(noveGrupe[i])
//            }
        }

        suspend fun AzurirajPredmete() {
            try {
                var sviPr = PredmetIGrupaRepository.getPredmeti()
                var mojeGrupe = PredmetIGrupaRepository.getUpisaneGrupe()
                var rez = sviPr.filter { p ->
                    PredmetIGrupaRepository.getGrupeZaPredmet(p.id)!!
                        .find { g -> mojeGrupe!!.contains(g) } != null
                }

                var db = AppDatabase.getInstance(context)
                var predmetDao = db.predmetDao()
                predmetDao.obrisi()
//                for (i in rez!!.indices) {
//                    predmetDao.insertAll(rez[i])
//                }
            } catch (e: NullPointerException) {
            }
        }

        suspend fun AzurirajKvizove() {
            var accRepozitory = AccountRepository
               accRepozitory.setContext(context)
            var grupe = ApiConfig.retrofit.getGrupeZaStudenta(accRepozitory.getHash())
            var listaKvizova: MutableList<Kviz> = mutableListOf()
                    grupe.body()?.forEach { grupa ->
                            var kvizoviZaGrupu = ApiConfig.retrofit.getKvizoviZaGrupu(grupa.id).body()
                            kvizoviZaGrupu?.forEach { kviz -> listaKvizova.add(kviz) }

                    }

            var db = AppDatabase.getInstance(context)
            var kvizDao = db.kvizDao()
            kvizDao.obrisi()
//            for (i in listaKvizova!!.indices) {
//                kvizDao.insertAll(listaKvizova[i])
//            }
        }

        suspend fun AzurirajPitanja() {
//            var db = AppDatabase.getInstance(context)
//            var pitanjeDao = db.pitanjeDao()
 //           pitanjeDao.obrisi()
//            var kvizDao = db.kvizDao()
//            var mojiKvizovi = kvizDao.dajSveMojeKvizove() //azurirali smo prvo kvizove pa treba da je okej
//            for (i in mojiKvizovi!!.indices) {
//                for(j in mojiKvizovi[i].)
//                pitanjeDao.insertAll()
//            }
        }

    }
}