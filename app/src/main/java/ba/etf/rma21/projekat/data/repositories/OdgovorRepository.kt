package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.RequestBody
import java.lang.IllegalStateException
import java.net.MalformedURLException
import java.text.SimpleDateFormat
import java.util.*


class OdgovorRepository {


    companion object {
        private var grupaViewModel = GrupaViewModel()
        private var listOdgovorPitanje: MutableList<OdgovorPitanje> = mutableListOf()

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        private suspend fun nadjiMiIdKvizTaken(idKviza: Int): Int?{
            try{
            var sviKvizTaken = ApiConfig.retrofit.getPokusajiZaKvizoveStudenta(AccountRepository.getHash()).body()
            return sviKvizTaken?.find { kt-> kt.KvizId==idKviza }?.id
                }catch (e: IllegalStateException){ return -1 }
        }

        suspend fun predajOdgovore(idKviz: Int){
             var db = AppDatabase.getInstance(context)
            var odgovorDao = db.odgovorDao()
            var sviOdgovori = odgovorDao.getAll()
            var pitanjaDosad: MutableList<Pitanje> = mutableListOf()
            for(i in sviOdgovori.indices){
                if(sviOdgovori[i].idKviz == idKviz ){
                    var ktid = sviOdgovori[i].kvizTakenId
                    var pitanje = db.pitanjeDao().dajPitanjeSaId(sviOdgovori[i].idPitanje)
                    pitanjaDosad.add(pitanje)
                    var bodovi = sracunajBodoveZaKvizOdgovori(pitanjaDosad, sviOdgovori)
                    var odgPitanje = OdgovorPitanje(sviOdgovori[i].odgovoreno, sviOdgovori[i].idPitanje, bodovi)
                    ApiConfig.retrofit.dodajOdgovorPokusajKvizStudent(AccountRepository.getHash(), ktid, odgPitanje )
                }
            }
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
            db.kvizDao().postaviPredanIDatumRada(idKviz,true, formatter.format(Calendar.getInstance().time)  )

        }

        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                try {
                    if (KvizRepository.getUpisani().find { kviz -> kviz.id == idKviza } == null) return@withContext emptyList<Odgovor>()
                    var listaPocetih = TakeKvizRepository.getPocetiKvizovi()!!
                    if (listaPocetih.find { kvizTaken -> kvizTaken.KvizId == idKviza  } == null ) return@withContext emptyList<Odgovor>()
                    var idKvizTaken = nadjiMiIdKvizTaken(idKviza)
                    if (idKvizTaken == null) return@withContext emptyList<Odgovor>()
                    var response = ApiConfig.retrofit.pokusajKvizStudent(AccountRepository.getHash(), idKvizTaken!!).body()
                    return@withContext response!!
                }catch(e: IllegalStateException){
                    return@withContext emptyList<Odgovor>()
                }
            }
        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int {
            Log.i("kviz koji trazim id: ", "ispod")
            //vraca ukupne bodove NA KVIZU nakon odgovora
            return withContext(Dispatchers.IO) {
               try {
                   Log.i("kviz koji trazim id: ", "ispod")
                   var db = AppDatabase.getInstance(context)
                   var odgovorDao = db.odgovorDao()
                   var kvizDao = db.kvizDao()
                    var hash = AccountRepository.getHash()
                   var pokusajiKvizova = TakeKvizRepository.listaPocetihKvizovaId

                   var kvizKojiTrazim = kvizDao.dajSveMojeKvizove().find { k->k.ktid == idKvizTaken }
                   Log.i("kviz koji trazim id: ", "test")
                   var pitanjaNaKvizu:MutableList<Pitanje> = mutableListOf()
                   if(kvizKojiTrazim!=null)    pitanjaNaKvizu=db.pitanjeDao().dajSvaPitanja().toMutableList()   //dajPitanjaNaKvizu(kvizKojiTrazim!!.id).toMutableList()
                   var trazenoPitanje = pitanjaNaKvizu?.find { pitanje -> pitanje.id == idPitanje }
                    Log.i("vraca li nulu: ", "da")
                   // if (pitanjaNaKvizu.size == 0) return@withContext 0
                    Log.i("vraca li nulu: ", "ne")
                    var sljedeciId = odgovorDao.getAll().size
                   if(kvizKojiTrazim!=null) {
                       if (odgovorDao.getAll()
                               .find { o -> o.idKviz == kvizKojiTrazim!!.id && o.idPitanje == idPitanje } == null
                       ) {
                           odgovorDao.dodajOdgovor(
                               sljedeciId,
                               odgovor,
                               idPitanje,
                               kvizKojiTrazim!!.id,
                               idKvizTaken
                           )
                       }
                   }


                    var bodovi = 0 //bodovi u OdgovorPitanje su bodovi NAKON odgovora tj. osvojeni
                    if (trazenoPitanje!!.tacan == odgovor) {
                        bodovi++
                    }
                    var odgovorObjekt = OdgovorPitanje(odgovor, idPitanje, bodovi)
                    listOdgovorPitanje.add(odgovorObjekt)
                    //prolazimo kroz listu dosad nadjenih odgovorObjekata
                    // za svaki objekt koji ima pitanje ciji se id slaze sa
                    // nekim id-em pitanja sa pitanjaNaKvizu za trazeni kviz
                    // sabiramo bodove... na kraju je ukupni broj bodova na
                    // kvizu jednak sabrani/pitanjaNaKvizu.size() (pod pretpostavkom da svako pitanje nosi 1b)
                    var tacnoOdgovorenih = sracunajBodoveZaKviz(pitanjaNaKvizu)
                    Log.i("odgRepo: tacno odg = ", tacnoOdgovorenih.toString())
                    Log.i("odgRepo: pitanja.size = ", pitanjaNaKvizu.size.toString() )

                    return@withContext ((tacnoOdgovorenih.toDouble() / pitanjaNaKvizu.size) * 100).toInt()
                }catch (e: NullPointerException){
                    Log.i("exception odgovor ", e.toString())
                    return@withContext 0
                }
            }
        }

        fun sracunajBodoveZaKviz(pitanjaNaKvizu: List<Pitanje>): Int{
            var bodovi=0
            for(i in listOdgovorPitanje.indices){
                var idPitanja = listOdgovorPitanje[i].pitanje
                var imaLiPitanja = pitanjaNaKvizu.find { pitanje->pitanje.id == idPitanja }
                if(imaLiPitanja!=null) bodovi++
            }
            return bodovi
        }

       fun sracunajBodoveZaKvizOdgovori(pitanjaNaKvizu: List<Pitanje>, odgovori: List<Odgovor>): Int{
            var bodovi=0
            for(i in odgovori.indices){
                var idPitanja = odgovori[i].idPitanje
                var imaLiPitanja = pitanjaNaKvizu.find { pitanje->pitanje.id == idPitanja }
                if(imaLiPitanja!=null) bodovi++
        }
            return bodovi
     }
    }


}