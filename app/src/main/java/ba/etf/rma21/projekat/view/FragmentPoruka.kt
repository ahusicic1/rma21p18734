package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R

class FragmentPoruka: Fragment() {
   private lateinit var tvPoruka: TextView
    companion object {
        fun newInstance(): FragmentPoruka = FragmentPoruka()
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.poruka_fragment, container, false)
        tvPoruka = view.findViewById<TextView>(R.id.tvPoruka)
        val grupa = arguments?.getString("grupa")
        val predmet = arguments?.getString("predmet")
        val kvizNaziv = arguments?.getString("nazivKviza")
        val procenatT = arguments?.getString("procenatTacnost")


        if(kvizNaziv == null || kvizNaziv == "") {
            tvPoruka.text = "Uspješno ste upisani u grupu $grupa predmeta $predmet!"
        }
        else{
            tvPoruka.text = "Završili ste kviz $kvizNaziv sa tačnosti $procenatT"
        }

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                (activity as MainActivity).supportFragmentManager.beginTransaction().replace(R.id.container, FragmentKvizovi.newInstance()).addToBackStack(null).commit()
                (activity as MainActivity).odaberiOpcijuBottomNav(R.id.kvizovi)
            }
        })

        return view
    }
}