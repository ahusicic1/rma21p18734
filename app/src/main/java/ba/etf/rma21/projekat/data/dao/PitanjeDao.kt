package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.Predmet

@Dao
interface PitanjeDao {
    @Query("SELECT * FROM pitanje WHERE kvizId=:kid")
    suspend fun dajPitanjaNaKvizu( kid: Int): List<Pitanje>

    @Query("SELECT * FROM pitanje")
    suspend fun dajSvaPitanja( ): List<Pitanje>

//    @Query("INSERT INTO pitanje(id, naziv, tekstPitanja, opcije, tacan, kvizId) VALUES(:id, :naziv, :tekst, :opcije, :tacan, :kid)")
//    suspend fun insertAll(id:Int, naziv:String, tekst:String, opcije:String, tacan:Int, kid:Int)

    @Insert
    suspend fun insertAllPitanje(vararg pitanje: Pitanje)

    @Query("DELETE FROM pitanje")
    suspend fun obrisi()

    @Query("SELECT * FROM pitanje WHERE id=:id LIMIT 1")
    suspend fun dajPitanjeSaId(id:Int): Pitanje
}