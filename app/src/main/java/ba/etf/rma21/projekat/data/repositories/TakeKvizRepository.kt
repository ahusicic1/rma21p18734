package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.IllegalStateException
import java.text.SimpleDateFormat
import java.util.*

class TakeKvizRepository {
    companion object {
        var listaPocetihKvizovaId:MutableList<Int> = mutableListOf()
        var listaPocetihKvizova:MutableList<KvizTaken> = mutableListOf()

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun zapocniKviz(idKviza: Int): KvizTaken {
             return withContext(Dispatchers.IO) {
                  context=AccountRepository.getContext()
                 var db = AppDatabase.getInstance(context)
                 var kvizDao = db.kvizDao()
//                 kvizDao.zapocniKviz(idKviza, "true")
                 var kvizPoceti = kvizDao.dajSveMojeKvizove().find { k->k.id==idKviza }

                if(kvizPoceti!=null) KvizListViewModel.pocetiKvizovi.add(kvizPoceti!!)
                var response = ApiConfig.retrofit.zapocniOdgovaranje(AccountRepository.getHash(), idKviza)
                 var ktid = response.body()!!.id
                 kvizDao.postaviKvizTakenId(idKviza, ktid)
                 listaPocetihKvizova.add(response.body()!!)
                 if(response.isSuccessful){
                     listaPocetihKvizovaId.add(idKviza)
                 }

                return@withContext response.body()!!
            }
        }


        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
//            var response: List<KvizTaken> = withContext(Dispatchers.IO){
//                return@withContext ApiConfig.retrofit.getPokusajiZaKvizoveStudenta(AccountRepository.getHash()!!)
//            }
//            if(response.isEmpty()) return null
//            return response
            return withContext(Dispatchers.IO) {
                try {
                  return@withContext listaPocetihKvizova.toList()
                }catch (e: IllegalStateException){
                    return@withContext emptyList<KvizTaken>()
                }
            }
        }
        suspend fun getPocetiKvizovi_2(): List<Kviz>? {
            return withContext(Dispatchers.IO) {
                try {
                    var db = AppDatabase.getInstance(context)
                    var kvizDao = db.kvizDao()
                    var response = kvizDao.dajSveMojeKvizove().filter { kviz->kviz.zapocet!=null && kviz.zapocet=="true" }
                    if (response == null || response.size == 0) return@withContext null
                    KvizListViewModel.pocetiKvizovi = response.toMutableList()
                    return@withContext response!!
                }catch (e: IllegalStateException){
                    return@withContext emptyList<Kviz>()
                }
            }
        }
    }


    }

